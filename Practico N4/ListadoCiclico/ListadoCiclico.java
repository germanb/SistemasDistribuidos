package examples.ListadoCiclico;
import jade.core.*;
import java.io.*;
import jade.core.behaviours.CyclicBehaviour;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.Thread;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.lang.Math;

public class ListadoCiclico extends Agent{
	String strdir = "/home/emiliano/";                // Dir to list
	String[] list;
	ContainerID destino = null;
	Location origen = null;
	private int availableProcessors = ManagementFactory.getOperatingSystemMXBean().getAvailableProcessors();
	private long lastSystemTime = 0;
	private long lastProcessCpuTime = 0;

	//Impresion del Hostname
	public void imprimirHostname(){
		String hostname = "Unknown";
		try{
				InetAddress addr;
				addr = InetAddress.getLocalHost();
				hostname = addr.getHostName();
		}
		catch (UnknownHostException ex){
				System.out.println("Hostname can not be resolved");
		}
		System.out.println("Hostname: " + hostname);
	}

	//Impresion de la carpeta home
	public void impimirHome(){
		try {
			File dir = new File(strdir);
			list = dir.list();
			System.out.println("recuperó el directorio en " + here().getID());
			for (int i = 0; i < list.length; i++)
				System.out.println(i + ": " + list[i]);

		}catch (Exception e){
			System.out.println("Falló al recuperar directorio dir.list()");
			e.getMessage();
		}
	}

	//PARA PERFORMANCE Y CARACTERISTICAS GENERALES
	public synchronized String getStats() {
    java.lang.management.OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
    com.sun.management.OperatingSystemMXBean osDetail = null;
    if ( os instanceof com.sun.management.OperatingSystemMXBean ) {
      osDetail = (com.sun.management.OperatingSystemMXBean)os;
    }
    StringBuffer stats = new StringBuffer();
    stats.append( "operating system architecture: " + osDetail.getArch() + "\n" );
    stats.append( "processors available to the Java virtual machine: " + osDetail.getAvailableProcessors() + "\n" );
    stats.append( "virtual memory guaranteed to the running process in Mb: " + osDetail.getCommittedVirtualMemorySize() / (1024 * 1024) + "\n" );
    stats.append( "amount of free physical memory in Mb: " + osDetail.getFreePhysicalMemorySize() / (1024 * 1024) + "\n" );
    stats.append( "amount of free swap space in bytes: " + osDetail.getFreeSwapSpaceSize()/ (1024 * 1024) + "\n" );
    stats.append( "operating system name: " + osDetail.getName() + "\n" );
    stats.append( "jvm process CPU time in seconds: " + osDetail.getProcessCpuTime() / (1000 * 1000) + "\n" );
    stats.append( "system load average for the last minute: " + osDetail.getSystemLoadAverage() + "\n" );
    stats.append( "total amount of physical memory in Mb: " + osDetail.getTotalPhysicalMemorySize()/ (1024 * 1024) + "\n" );
    stats.append( "total amount of swap space in Mb: " + osDetail.getTotalSwapSpaceSize()/ (1024 * 1024) + "\n" );
    stats.append( "operating system version: " + osDetail.getVersion() + "\n" );
    stats.append( "Total Heap Memory bytes: " + java.lang.Runtime.getRuntime().totalMemory() + "\n" );
    stats.append( "Free Heap Memory bytes: " + java.lang.Runtime.getRuntime().freeMemory() + "\n" );
    return stats.toString();
  }/*
  public synchronized long getHeapSpace() {
    java.lang.Runtime runtime = java.lang.Runtime.getRuntime();
    return java.lang.Runtime.maxMemory();
  }
  public synchronized long getFreeHeap() {
    java.lang.Runtime runtime = java.lang.Runtime.getRuntime();
    return java.lang.Runtime.freeMemory() + java.lang.Runtime.maxMemory() - java.lang.Runtime.totalMemory();
  }*/
  public synchronized double getCpuUsage() {
    if ( lastSystemTime == 0 ) {
      baselineCounters();
      return -1.0;
    }
    long systemTime = System.nanoTime();
    long processCpuTime = 0;
    if ( ManagementFactory.getOperatingSystemMXBean() instanceof OperatingSystemMXBean ) {
      processCpuTime = ( (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean() ).getProcessCpuTime();
    }
    double cpuUsage = (double) ( processCpuTime - lastProcessCpuTime ) / ( systemTime - lastSystemTime );
    lastSystemTime = systemTime;
    lastProcessCpuTime = processCpuTime;
    return cpuUsage / availableProcessors;
  }
  private void baselineCounters() {
    lastSystemTime = System.nanoTime();
    if ( ManagementFactory.getOperatingSystemMXBean() instanceof OperatingSystemMXBean ) {
      lastProcessCpuTime = ( (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean() ).getProcessCpuTime();
    }
  }
  public void imprimirPerformance(){
		getCpuUsage();
		List<Calendar> list = new ArrayList<Calendar>();
    for ( int i = 0; i < 1000000000; i++ ) {
      // Petar la lista con 1000 calendars
      for ( int k = 0; k < 10000; k++ ){ list.add( new GregorianCalendar() ); }
      // Petar la CPU con ciclos vacios
      for ( int j = 0; j< 1000000000; j++ ) {}
      // Mem en Mb
			//System.out.println( "CPU: " + this.getCpuUsage() + "\t\tMem: " + this.getFreeHeap() / (1024d*1024d) + " (" + this.getHeapSpace() / (1024d*1024d) + ")" );
      System.out.println( "CPU: " + this.getCpuUsage() );
			this.getStats();
    }
  }

	//Impresion de la Informacion
	public void imprimirInfo(){
		Calendar horaLlegada = Calendar.getInstance();
		System.out.println(horaLlegada);
		long millisLlegada = horaLlegada.getTimeInMillis();
		this.imprimirHostname();
		this.imprimirPerformance();
		Calendar horaSalida = Calendar.getInstance();
		long millisSalida = horaSalida.getTimeInMillis();
		long tiempoEnHost = millisSalida - millisLlegada;
		if (tiempoEnHost < 10000) {
			int faltante = java.lang.Math.toIntExact(10000 - tiempoEnHost);
			Thread.sleep(faltante);
			tiempoEnHost = tiempoEnHost + faltante;
		}
		System.out.println("Tiempo en el host: " + tiempoEnHost);
	}

	//FUNCIONAMIENTO GENERAL DE LAS MIGRACIONES DE AGENTES
	public void setup()
	{
		System.out.println("Se crea al agente --> " + getName());
		destino = new ContainerID("Container-1", null);
		System.out.println("Destino --> " + destino.getID());
		origen = here();
		System.out.println("Origen --> " + origen.getID());
		// registra el comportamiento deseado del agente
		addBehaviour(new CyclicBehaviour(this){
			public void action() {
				switch(_state){
				case 0:
					_state++;
					//Imprime antes de migrar
					this.imprimirInfo();
					// Comienza la migración del agente al destino
					System.out.println("Estado 0 Comienza la migración del agente al destino --> " + destino.getID());
					try {
						doMove(destino);
						System.out.println("Despues de doMove en CyclicBehaviour de Estado 0 --> " + destino.getID());
					} catch (Exception e) {
						System.out.println("fallo al moverse al Container destino");
						e.getMessage();
					}
					break;
				case 1:
				// el agente llegó al destino, recupera el directorio y regresa
					_state++;
					System.out.println("Estado 1 agente llegó a destino, recupera directorio y regresa a --> " + origen.getID());

					this.imprimirInfo();
					// regresa al origen e imprime el directorio
					try {
						System.out.println("regresando a --> " + origen.getID());
						doMove(origen);
						System.out.println("despues de domove en CyclicBehaviour estado 1 --> " + here().getID());
					} catch (Exception e) {
						System.out.println("Falla al mover al regresar al origen");
						e.getMessage();
					}
					break;
				case 2:
					// Regresó al origen, imprime el directorio y destruye al agente
					System.out.println("Estado 2 Regresó a origen, imprime directorio y destruye agente --> " + here().getID());
					for (int i = 0; i < list.length; i++)
					System.out.println(i + ": " + list[i]);
					// destruye al agente

					System.out.println("destruye al agente --> " + getName());
					doDelete();
					break;
				default:
					myAgent.doDelete();
				}
			}
			private int _state = 0; // variable de máquina de estados del agente
		});

		// registra un comportamiento dummy a los efectos de verificar concurrencia y movilidad
		addBehaviour(new CyclicBehaviour(this){
			public void action()
			{
				// arranca muestra cartel, duerme 5 segundos y muestra otro cartel
				_contador++;
				System.out.println("Behaviour dummy antes de dormir ciclo--> " + _contador + " --> " + here().getID());
				try {
					Thread.sleep(5000);
				} catch (InterruptedException ex) {
					Logger.getLogger(ListadoCiclico.class.getName()).log(Level.SEVERE, null, ex);
				}
				System.out.println("Behaviour dummy despues de dormir ciclo--> " + _contador + " --> " + here().getID());
			}
			private int _contador = 0; // cuenta la cantidad de ciclos en que se ejecuta el comportamiento
		});
	}

	// Luego de ser movido el agente ejecuta este código
	protected void afterMove() {
		System.out.println("Siempre ejecuta afterMove cuando al llegar --> " + here().getID());
	}
}
