package Punto2Agent;

import jade.core.*;
import jade.core.behaviours.CyclicBehaviour;
import java.util.*; 
import jade.lang.acl.*;
import jade.content.*;
import jade.content.onto.basic.*;
import jade.content.lang.sl.*;
import jade.domain.JADEAgentManagement.*;
import jade.domain.mobility.*;


class Punto2Agent extends Agent {
    
    public void setup() {
        getContentManager().registerLanguage(new SLCodec());
    }
}