package DispContainers;

import jade.core.*;
import jade.core.behaviours.CyclicBehaviour;
import java.util.*; 
import jade.lang.acl.*;
import jade.content.*;
import jade.content.onto.basic.*;
import jade.content.lang.sl.*;
import jade.domain.JADEAgentManagement.*;
import jade.domain.mobility.*;
import java.lang.Thread;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.lang.Math;


public class DispContainers extends Agent {
	private boolean error = false;
	private int operacion = 0;

	private ArrayList containers = new ArrayList(); // Obtiene una Lista de los Contenedores registrados en JADE.
	private int cantidad_maxima_contenedores = 0;

    // **********************************************************************************************************
	// ContainerID destino = null;
	Location destino= null;
	Location origen = null;
	Location mudarse = null;


	public void setup() {
		System.out.println("Se crea al agente --> " + getName()+"\n");

		// Registramos el lenguaje y ontologia para la movilidad del agente.
		getContentManager().registerLanguage(new SLCodec());
	    getContentManager().registerOntology(MobilityOntology.getInstance());

	    origen = here();
		System.out.println("Origen --> " + origen.getName()+"\n");

		// Registra el comportamiento deseado del agente
		addBehaviour(
			new CyclicBehaviour(this) {
				private ArrayList<String> informaciones = new ArrayList<String>();
				private int indice_contenedor = 0;

				//FUNCIONES
				//*******************************************************************************
				private int availableProcessors = ManagementFactory.getOperatingSystemMXBean().getAvailableProcessors();
				private long lastSystemTime = 0;
				private long lastProcessCpuTime = 0;

				//Impresion del Hostname
				public String getHostname(){
					String hostname = "Unknown";
					try{
							InetAddress addr;
							addr = InetAddress.getLocalHost();
							hostname = addr.getHostName();
					}
					catch (UnknownHostException ex){
							return "Hostname can not be resolved";
					}
					return "Hostname: " + hostname;
				}

				//PARA PERFORMANCE Y CARACTERISTICAS GENERALES
				public synchronized StringBuffer getStats() {
					java.lang.management.OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
					com.sun.management.OperatingSystemMXBean osDetail = null;
					if ( os instanceof com.sun.management.OperatingSystemMXBean ) {
						osDetail = (com.sun.management.OperatingSystemMXBean)os;
					}
					StringBuffer stats = new StringBuffer();
					stats.append( "\n*****************************************************************************************************\n\n"); 
					stats.append( "Host: " + this.getHostname() + "\n" );
					stats.append( "Sistema Operativo: " + osDetail.getName() + "\n" );					
					stats.append( "Version del Sistema Operativo: " + osDetail.getVersion() + "\n" );					
					stats.append( "Arquitectura del Sistema Operativo: " + osDetail.getArch() + "\n" );
					stats.append( "Procesadores diponibles para la maquina virtual de Java: " + osDetail.getAvailableProcessors() + "\n" );
					stats.append( "Tiempo de procesamiento del CPU JVM en segundos: " + osDetail.getProcessCpuTime() / (1000 * 1000) + "\n" );					
					stats.append( "Memoria virtual garantizada para el proceso en ejecucion en MB: " + osDetail.getCommittedVirtualMemorySize() / (1024 * 1024) + "\n" );
					stats.append( "Espacio total de la memoria fisica en MB: " + osDetail.getTotalPhysicalMemorySize()/ (1024 * 1024) + "\n" );
					stats.append( "Espacio libre de memoria fisica en MB: " + osDetail.getFreePhysicalMemorySize() / (1024 * 1024) + "\n" );
					stats.append( "Espacio total para swap en MB: " + osDetail.getTotalSwapSpaceSize()/ (1024 * 1024) + "\n" );
					stats.append( "Espacio libre para swap en bytes: " + osDetail.getFreeSwapSpaceSize()/ (1024 * 1024) + "\n" );
					stats.append( "Promedio de carga del sistema en el ultimo minuto: " + osDetail.getSystemLoadAverage() + "\n" );
					stats.append( "Tamaño total del Heap en bytes: " + java.lang.Runtime.getRuntime().totalMemory() + "\n" );
					stats.append( "Espacio libre del Heap en bytes: " + java.lang.Runtime.getRuntime().freeMemory() + "\n" );
					stats.append( "Uso de la CPU: " + this.getCpuUsage() + "\n");
					
					return stats;
				}

				public synchronized double getCpuUsage() {
					if ( lastSystemTime == 0 ) {
						baselineCounters();
						return -1.0;
					}
					long systemTime = System.nanoTime();
					long processCpuTime = 0;
					if ( ManagementFactory.getOperatingSystemMXBean() instanceof OperatingSystemMXBean ) {
						processCpuTime = ( (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean() ).getProcessCpuTime();
					}
					double cpuUsage = (double) ( processCpuTime - lastProcessCpuTime ) / ( systemTime - lastSystemTime );
					lastSystemTime = systemTime;
					lastProcessCpuTime = processCpuTime;
					return cpuUsage / availableProcessors;
				}

				private void baselineCounters() {
					lastSystemTime = System.nanoTime();
					if ( ManagementFactory.getOperatingSystemMXBean() instanceof OperatingSystemMXBean ) {
						lastProcessCpuTime = ( (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean() ).getProcessCpuTime();
					}
				}

				// Impresion de la Informacion
				public synchronized String getInfo(){
					Calendar horaLlegada = Calendar.getInstance();
					
					StringBuffer info = this.getStats();
					info.append("Hora de llegada: " + horaLlegada.get(Calendar.HOUR) + ":" + horaLlegada.get(Calendar.MINUTE));
					long millisLlegada = horaLlegada.getTimeInMillis();
					Calendar horaSalida = Calendar.getInstance();
					long millisSalida = horaSalida.getTimeInMillis();
					long tiempoEnHost = millisSalida - millisLlegada;
					if (tiempoEnHost < 10000) {
						int faltante = java.lang.Math.toIntExact(10000 - tiempoEnHost);
						try{
							Thread.sleep(faltante);
						}catch (InterruptedException e){
							e.printStackTrace();
						}
						tiempoEnHost = tiempoEnHost + faltante;
					}
					info.append("Tiempo en el host: " + tiempoEnHost);
					info.append( "\n*****************************************************************************************************\n\n");		
					return info.toString();
				}

				//*******************************************************************************
				public void action() {
					if (_state == 0) {
						verContainers();
						cantidad_maxima_contenedores = containers.size();
						System.out.println("Cantidad de contenedores: " + cantidad_maxima_contenedores);
						if (cantidad_maxima_contenedores != 0)
							_state = 1;
						else
							_state = 2;
					}
					switch(_state) {
						case 1:
							System.out.println("Indice antes de entrar: " + indice_contenedor);
							destino = (Location)containers.get(indice_contenedor);
							System.out.println("Destino actual: " + destino);
							++indice_contenedor;
							System.out.println("Indice despues de incrementar: "+indice_contenedor);
							if (indice_contenedor == cantidad_maxima_contenedores)
								_state = 2;

							informaciones.add(this.getInfo());
							
							System.out.println("Estado 1 Comienza la migracion del agente al destino --> " + destino.getName()+"\n");
							try {
								doMove(destino);
								System.out.println("Despues de doMove en CyclicBehaviour de Estado 1 --> " + destino.getName()+"\n");
							} catch (Exception e) {
								System.out.println("fallo al moverse \n");
								e.getMessage();
							}
							break;
						case 2:							
							System.out.println("Estado 4 Comienza la auto eliminacion del agente en origen --> " + getName()+"\n");
							try {
								System.out.println("### Se recorrieron " + informaciones.size() + " contenedores ###");
								for (int i = 0; i < informaciones.size(); i++) {
									System.out.println(informaciones.get(i));
								}
								doDelete();
								System.out.println("Despues de la auto eliminacion del agente Estado 4 --> " + getName() +"\n");
							} catch (Exception e) {
								System.out.println("fallo al moverse al Container-1 \n");
								e.getMessage();
							}
							break; 									
					}
				}
				private int _state = 0; // Variable de maquina de estados del agente
			}
		);
	}

	//Metodo para actualizar a lista de containers disponibles
	protected void actualizarContainers() {	 	
		getContentManager().registerLanguage(new SLCodec());
	    getContentManager().registerOntology(MobilityOntology.getInstance());

	    //origen = here();
	    containers.clear();
	    ACLMessage request= new ACLMessage(ACLMessage.REQUEST);
	    request.setLanguage(new SLCodec().getName());

	    // Establecemos que MobilityOntology sea la ontologia de este mensaje.
	    request.setOntology(MobilityOntology.getInstance().getName());

	    // Solicitamos a AMS una lista de containers disponibles
	    Action action= new Action(getAMS(), new QueryPlatformLocationsAction());

	    try {
	      getContentManager().fillContent(request, action);
	      request.addReceiver(action.getActor());
	      send(request);

	      // Filtramos los mensajes INFORM que llegan desde el AMS
	      MessageTemplate mt= MessageTemplate.and(MessageTemplate.MatchSender(getAMS()), MessageTemplate.MatchPerformative(ACLMessage.INFORM));

	      ACLMessage resp= blockingReceive(mt);
	      ContentElement ce= getContentManager().extractContent(resp);
	      Result result=(Result) ce;
	      jade.util.leap.Iterator it= result.getItems().iterator();
	      // Almacena un ArrayList "Locations" de "Containers" donde puede moverse el agente movil.

	      while(it.hasNext()) 
	      {
		    Location loc=(Location) it.next();
		    containers.add(loc);
	      }
	    } catch(Exception ex) {  
			ex.printStackTrace();
		}
	}	

	protected void verContainers() {
	    //ACTUALIZAR
	    actualizarContainers();
	    //VISUALIZAR
	    System.out.println("******Containers disponibles: *******\n");
	    for(int i=0; i<containers.size(); i++) {
	      System.out.println("    ["+ i + "] " + ((Location)containers.get(i)).getName());
	    }
	    System.out.println("\n");
	 }
}
