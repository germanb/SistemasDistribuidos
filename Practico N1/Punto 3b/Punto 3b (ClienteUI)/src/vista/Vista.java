package vista;

import java.awt.EventQueue;
import javax.swing.JFrame;
import controlador.Controlador;
import modelo.Cliente;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

public class Vista {
	private JFrame frmSistemasDistribuidos;
	private Cliente modelo;
	private Controlador controlador;
	// Elementos de la caja de conexiones
	private JTextField text_ip;
	private JTextField text_puerto;
	private JButton btnConectar;
	private JButton btnDesconectar;
	// Elementos de la caja de acciones
	private JTextField text_abrir_remoto;
	private JButton btnAbrirRemoto;
	private JComboBox<comboItem> combo_cerrar_remoto;
	private JButton btnCerrarRemoto;
	private JComboBox<comboItem> combo_rw;
	private JTextPane text_texto;
	private JButton btnLeer;
	private JButton btnEscribir;
	// Elementos de la caja de arhivos
	private JTextField txt_abrir_local;
	private JButton btnAbrirLocal;
	private JComboBox<comboItem> combo_cerrar_local;
	private JButton btnCerrarLocal;
	private JComboBox<comboItem> combo_archivo_local;
	private JComboBox<comboItem> combo_archivo_remoto;
	private JButton btnLocalARemoto;
	private JButton btnRemotoALocal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista window = new Vista();
					window.frmSistemasDistribuidos.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Vista() {
		this.modelo = new Cliente();
		this.controlador = new Controlador(this, this.modelo);
		initialize();
	}
	
	/* CAJA DE CONEXION */
	public String getIP() {
		return this.text_ip.getText();
	}
	
	public void setIP(String ip) {
		this.text_ip.setText(ip);
	}
	
	public String getPuerto() {
		return this.text_puerto.getText();
	}
	
	public void setPuerto(String puerto) {
		this.text_puerto.setText(puerto);
	}
	
	/* CAJA DE LECTURA/ESCRITURA */
	public comboItem getCombo_rw() {
		return (comboItem) this.combo_rw.getSelectedItem();
	}
	
	public void setCombo_rw(int fd, String nombre, String tipo) {
		this.combo_rw.addItem(new comboItem(fd, nombre, tipo));
	}
	
	public void borraItem_comboRW(int fd, String nombre, String tipo) {
		int cantidad = this.combo_rw.getItemCount();
		
		for (int i = 0; i < cantidad; i++) {
			comboItem item = this.combo_rw.getItemAt(i);
			
			if (item.getFd() == fd) {
				if (item.getTipo() == tipo) {
					this.combo_rw.removeItemAt(i);
				}
			}
		}
	}
	
	public void actualiza_texto(String texto) {
		this.text_texto.setText(texto);
	}
	
	public String obtiene_texto() {
		return this.text_texto.getText();
	}
	
	/* CAJAS DE ARCHIVOS REMOTOS */
	public String getText_abrir_remoto() {
		return this.text_abrir_remoto.getText();
	}
	
	public void setText_abrir_remoto(String texto) {
		this.text_abrir_remoto.setText(texto);
	}
	
	public comboItem getCombo_cerrar_remoto() {
		return (comboItem) this.combo_cerrar_remoto.getSelectedItem();
	}
	
	public void agregaItem_ComboCerrar_remoto(int fd, String nombre) {
		String tipo = "Remoto";
		this.combo_cerrar_remoto.addItem(new comboItem(fd, nombre, tipo));
	}
	
	public void borraItem_comboCerrar_remoto(int indice) {
		comboItem item = this.combo_cerrar_remoto.getItemAt(indice);
		this.combo_cerrar_remoto.removeItem(item);
	}
	
	/* CAJAS DE ARCHIVOS LOCALES */
	public String getText_abrir_local() {
		return this.txt_abrir_local.getText();
	}
	
	public void setText_abrir_local(String archivo) {
		this.txt_abrir_local.setText(archivo);
	}
	
	public comboItem getCombo_cerrar_local() {
		return (comboItem) this.combo_cerrar_local.getSelectedItem();
	}
	
	public void agregaItem_ComboCerrar_local(int fd, String nombre) {
		String tipo = "Local";
		this.combo_cerrar_local.addItem(new comboItem(fd, nombre, tipo));
	}
	
	public void borraItem_comboCerrar_local(int indice) {
		comboItem item = this.combo_cerrar_local.getItemAt(indice);
		this.combo_cerrar_local.removeItem(item);
	}
	
	/* CAJA DE VOLCADO DE DATOS */
	public comboItem getCombo_archivo_local() {
		return (comboItem) this.combo_archivo_local.getSelectedItem();
	}
	
	public comboItem getCombo_archivo_remoto() {
		return (comboItem) this.combo_archivo_remoto.getSelectedItem();
	}
	
	public void agregaItem_combo_archivo_local(int fd, String nombre) {
		String tipo = "Local";
		this.combo_archivo_local.addItem(new comboItem(fd, nombre, tipo));
	}
	
	public void agregaItem_combo_archivo_remoto(int fd, String nombre) {
		String tipo = "Remoto";
		this.combo_archivo_remoto.addItem(new comboItem(fd, nombre, tipo));
	}
	
	public void borraItem_combo_archivo_local(int indice) {
		comboItem item = this.combo_archivo_local.getItemAt(indice);
		this.combo_archivo_local.removeItem(item);
	}
	
	public void borraItem_combo_archivo_remoto(int indice) {
		comboItem item = this.combo_archivo_remoto.getItemAt(indice);
		this.combo_archivo_remoto.removeItem(item);
	}
	
	/* OTROS METODOS */
	public void dialogo(String texto, String titulo, int flag) {
		JOptionPane.showMessageDialog(new JFrame(), texto, titulo, flag);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSistemasDistribuidos = new JFrame();
		frmSistemasDistribuidos.setTitle("Sistemas Distribuidos - Cliente RPC");
		frmSistemasDistribuidos.setBounds(100, 100, 727, 540);
		frmSistemasDistribuidos.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSistemasDistribuidos.getContentPane().setLayout(null);
		
		JPanel panel_conexion = new JPanel();
		panel_conexion.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		panel_conexion.setBounds(10, 11, 697, 36);
		frmSistemasDistribuidos.getContentPane().add(panel_conexion);
		panel_conexion.setLayout(null);
		
		JLabel lblDireccinIp = new JLabel("Direcci\u00F3n IP:");
		lblDireccinIp.setBounds(10, 11, 109, 14);
		panel_conexion.add(lblDireccinIp);
		
		text_ip = new JTextField();
		text_ip.setBounds(108, 8, 97, 20);
		panel_conexion.add(text_ip);
		text_ip.setColumns(10);
		
		JLabel lblPuerto = new JLabel("Puerto:");
		lblPuerto.setBounds(230, 11, 66, 14);
		panel_conexion.add(lblPuerto);
		
		text_puerto = new JTextField();
		text_puerto.setBounds(288, 8, 68, 20);
		panel_conexion.add(text_puerto);
		text_puerto.setColumns(10);
		
		btnDesconectar = new JButton("Desconectar");
		btnDesconectar.addActionListener(this.controlador);
		btnDesconectar.setActionCommand("Desconectar");
		btnDesconectar.setBounds(558, 7, 129, 23);
		panel_conexion.add(btnDesconectar);
		
		btnConectar = new JButton("Conectar");
		btnConectar.addActionListener(this.controlador);
		btnConectar.setActionCommand("Conectar");
		btnConectar.setBounds(439, 7, 109, 23);
		panel_conexion.add(btnConectar);
		
		JPanel panel_acciones = new JPanel();
		panel_acciones.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		// panel_acciones.setBounds(10, 58, 476, 402);
		panel_acciones.setBounds(10, 58, 697, 438);
		frmSistemasDistribuidos.getContentPane().add(panel_acciones);
		panel_acciones.setLayout(null);
		
		JLabel lblLabel = new JLabel("Acciones RFS");
		lblLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblLabel.setBounds(10, 11, 675, 14);
		panel_acciones.add(lblLabel);
		
		JPanel panel_rw = new JPanel();
		panel_rw.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		panel_rw.setBounds(10, 36, 365, 390);
		panel_acciones.add(panel_rw);
		panel_rw.setLayout(null);
		
		JLabel lblLeerescribirArchivos = new JLabel("Leer/Escribir Archivos");
		lblLeerescribirArchivos.setHorizontalAlignment(SwingConstants.CENTER);
		lblLeerescribirArchivos.setBounds(10, 11, 343, 14);
		panel_rw.add(lblLeerescribirArchivos);
		
		JLabel lblArchivo = new JLabel("Archivo:");
		lblArchivo.setBounds(20, 36, 66, 14);
		panel_rw.add(lblArchivo);
		
		combo_rw = new JComboBox<comboItem>();
		combo_rw.setBounds(84, 33, 154, 20);
		panel_rw.add(combo_rw);
		
		btnLeer = new JButton("Leer");
		btnLeer.addActionListener(this.controlador);
		btnLeer.setActionCommand("Leer");
		btnLeer.setBounds(10, 355, 89, 23);
		panel_rw.add(btnLeer);
		
		btnEscribir = new JButton("Escribir");
		btnEscribir.addActionListener(this.controlador);
		btnEscribir.setActionCommand("Escribir");
		btnEscribir.setBounds(254, 355, 99, 23);
		panel_rw.add(btnEscribir);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 62, 341, 281);
		panel_rw.add(scrollPane);
		
		text_texto = new JTextPane();
		scrollPane.setViewportView(text_texto);
		
		JPanel panel_abrir_remoto = new JPanel();
		panel_abrir_remoto.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		panel_abrir_remoto.setBounds(387, 36, 143, 130);
		panel_acciones.add(panel_abrir_remoto);
		panel_abrir_remoto.setLayout(null);
		
		JLabel lblAbrirArchivoRemoto = new JLabel("Abrir Archivo R");
		lblAbrirArchivoRemoto.setHorizontalAlignment(SwingConstants.CENTER);
		lblAbrirArchivoRemoto.setBounds(10, 11, 123, 14);
		panel_abrir_remoto.add(lblAbrirArchivoRemoto);
		
		JLabel lblNombreDelArchivoRemoto = new JLabel("Nombre:");
		lblNombreDelArchivoRemoto.setBounds(10, 40, 113, 14);
		panel_abrir_remoto.add(lblNombreDelArchivoRemoto);
		
		text_abrir_remoto = new JTextField();
		text_abrir_remoto.setBounds(10, 61, 123, 20);
		panel_abrir_remoto.add(text_abrir_remoto);
		text_abrir_remoto.setColumns(10);
		
		btnAbrirRemoto = new JButton("Abrir");
		btnAbrirRemoto.addActionListener(this.controlador);
		btnAbrirRemoto.setActionCommand("AbrirRemoto");
		btnAbrirRemoto.setBounds(28, 95, 89, 23);
		panel_abrir_remoto.add(btnAbrirRemoto);
		
		JPanel panel_cerrar_remoto = new JPanel();
		panel_cerrar_remoto.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		panel_cerrar_remoto.setBounds(542, 36, 143, 130);
		panel_acciones.add(panel_cerrar_remoto);
		panel_cerrar_remoto.setLayout(null);
		
		JLabel lblCerrarArchivoRemoto = new JLabel("Cerrar Archivo R");
		lblCerrarArchivoRemoto.setHorizontalAlignment(SwingConstants.CENTER);
		lblCerrarArchivoRemoto.setBounds(10, 11, 123, 14);
		panel_cerrar_remoto.add(lblCerrarArchivoRemoto);
		
		combo_cerrar_remoto = new JComboBox<comboItem>();
		combo_cerrar_remoto.setBounds(10, 61, 123, 20);
		panel_cerrar_remoto.add(combo_cerrar_remoto);
		
		JLabel lblArchivoRemoto = new JLabel("Archivo:");
		lblArchivoRemoto.setBounds(10, 40, 123, 14);
		panel_cerrar_remoto.add(lblArchivoRemoto);
		
		btnCerrarRemoto = new JButton("Cerrar");
		btnCerrarRemoto.addActionListener(this.controlador);
		btnCerrarRemoto.setActionCommand("CerrarRemoto");
		btnCerrarRemoto.setBounds(28, 95, 89, 23);
		panel_cerrar_remoto.add(btnCerrarRemoto);
		
		JPanel panel_abrir_local = new JPanel();
		panel_abrir_local.setLayout(null);
		panel_abrir_local.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		panel_abrir_local.setBounds(387, 178, 143, 130);
		panel_acciones.add(panel_abrir_local);
		
		JLabel lblAbrirArchivoLocal = new JLabel("Abrir Archivo L");
		lblAbrirArchivoLocal.setHorizontalAlignment(SwingConstants.CENTER);
		lblAbrirArchivoLocal.setBounds(10, 11, 123, 14);
		panel_abrir_local.add(lblAbrirArchivoLocal);
		
		JLabel lblNombreDelArchivoLocal = new JLabel("Nombre:");
		lblNombreDelArchivoLocal.setBounds(10, 40, 113, 14);
		panel_abrir_local.add(lblNombreDelArchivoLocal);
		
		txt_abrir_local = new JTextField();
		txt_abrir_local.setColumns(10);
		txt_abrir_local.setBounds(10, 61, 123, 20);
		panel_abrir_local.add(txt_abrir_local);
		
		btnAbrirLocal = new JButton("Abrir");
		btnAbrirLocal.addActionListener(this.controlador);
		btnAbrirLocal.setActionCommand("AbrirLocal");
		btnAbrirLocal.setBounds(28, 95, 89, 23);
		panel_abrir_local.add(btnAbrirLocal);
		
		JPanel panel_cerrar_local = new JPanel();
		panel_cerrar_local.setLayout(null);
		panel_cerrar_local.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		panel_cerrar_local.setBounds(542, 178, 143, 130);
		panel_acciones.add(panel_cerrar_local);
		
		JLabel lblCerrarArchivoLocal = new JLabel("Cerrar Archivo L");
		lblCerrarArchivoLocal.setHorizontalAlignment(SwingConstants.CENTER);
		lblCerrarArchivoLocal.setBounds(10, 11, 123, 14);
		panel_cerrar_local.add(lblCerrarArchivoLocal);
		
		combo_cerrar_local = new JComboBox<comboItem>();
		combo_cerrar_local.setBounds(10, 61, 123, 20);
		panel_cerrar_local.add(combo_cerrar_local);
		
		JLabel lblArchivoLocal = new JLabel("Archivo:");
		lblArchivoLocal.setBounds(10, 40, 123, 14);
		panel_cerrar_local.add(lblArchivoLocal);
		
		btnCerrarLocal = new JButton("Cerrar");
		btnCerrarLocal.addActionListener(this.controlador);
		btnCerrarLocal.setActionCommand("CerrarLocal");
		btnCerrarLocal.setBounds(28, 95, 89, 23);
		panel_cerrar_local.add(btnCerrarLocal);
		
		JPanel panel_volcado_contenido = new JPanel();
		panel_volcado_contenido.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		panel_volcado_contenido.setBounds(387, 320, 298, 106);
		panel_acciones.add(panel_volcado_contenido);
		panel_volcado_contenido.setLayout(null);
		
		JLabel lblVolcadoDeContenido = new JLabel("Volcado de Contenido");
		lblVolcadoDeContenido.setHorizontalAlignment(SwingConstants.CENTER);
		lblVolcadoDeContenido.setBounds(12, 12, 274, 15);
		panel_volcado_contenido.add(lblVolcadoDeContenido);
		
		combo_archivo_local = new JComboBox<comboItem>();
		combo_archivo_local.setBounds(12, 34, 133, 24);
		panel_volcado_contenido.add(combo_archivo_local);
		
		combo_archivo_remoto = new JComboBox<comboItem>();
		combo_archivo_remoto.setBounds(153, 34, 133, 24);
		panel_volcado_contenido.add(combo_archivo_remoto);
		
		btnLocalARemoto = new JButton("Local a Remoto");
		btnLocalARemoto.addActionListener(this.controlador);
		btnLocalARemoto.setActionCommand("VolcarLocalARemoto");
		btnLocalARemoto.setBounds(12, 70, 133, 25);
		panel_volcado_contenido.add(btnLocalARemoto);
		
		btnRemotoALocal = new JButton("Remoto a Local");
		btnRemotoALocal.addActionListener(this.controlador);
		btnRemotoALocal.setActionCommand("VolcarRemotoALocal");
		btnRemotoALocal.setBounds(153, 70, 133, 25);
		panel_volcado_contenido.add(btnRemotoALocal);
	}
	
	public class comboItem {
		private int fd;
		private String nombre;
		private String tipo;
		
		public comboItem(int fd, String nombre, String tipo) {
			this.fd = fd;
			this.nombre = nombre;
			this.tipo = tipo;
		}
		
		@Override
		public String toString() {
			return this.nombre + "(" + this.tipo + ")";
		}

		public int getFd() {
			return fd;
		}

		public void setFd(int fd) {
			this.fd = fd;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		
		public String getTipo() {
			return tipo;
		}
		
		public void setTipo(String tipo) {
			this.tipo = tipo;
		}
	}
}
