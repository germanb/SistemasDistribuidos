package modelo;

import java.io.File;
import java.net.*;
import java.util.*;

public class Cliente {
	String ip;
	int puerto;
	Socket sock;
	ArrayList<String> archivos_remotos_abiertos;
	ArrayList<File> archivos_locales_abiertos;
	
	// Constructor
	public Cliente() {
		this.ip = "localhost";
		this.puerto = 8080;
		this.archivos_remotos_abiertos = new ArrayList<String>();
		this.archivos_locales_abiertos = new ArrayList<File>();
	}
	
	// Getters y Setters
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPuerto() {
		return puerto;
	}

	public void setPuerto(int puerto) {
		this.puerto = puerto;
	}

	public Socket getSock() {
		return sock;
	}

	public void setSock(Socket sock) {
		this.sock = sock;
	}

	public ArrayList<String> getArchivos_remotos_abiertos() {
		return archivos_remotos_abiertos;
	}
	
	public ArrayList<File> getArchivos_locales_abiertos() {
		return archivos_locales_abiertos;
	}

	public void setArchivos_remotos_abiertos(ArrayList<String> archivos_abiertos) {
		this.archivos_remotos_abiertos = archivos_abiertos;
	}
	
	public void setArchivos_locales_abiertos(ArrayList<File> archivos_abiertos) {
		this.archivos_locales_abiertos = archivos_abiertos;
	}
	
	public void agrega_archivo_remoto_abierto(String archivo) {
		
		int cantidad = this.archivos_remotos_abiertos.size();
		boolean encontrado = false;
		for (int i = 0; i < cantidad; i++) {
			String nombre = this.archivos_remotos_abiertos.get(i);
			if (nombre == archivo) {
				encontrado = true;
			}
		}
		if (encontrado == false) {
			this.archivos_remotos_abiertos.add(archivo);	
		}
	}
	
	public void agrega_archivo_local_abierto(int indice, File archivo) {
		this.archivos_locales_abiertos.add(archivo);
	}
	
	public void quita_archivo_local_abierto(int indice) {
		// TODO quitar archivos por nombre (Y agregar de una vez que no se puda abrir el mismo archivo dos veces).
		this.archivos_locales_abiertos.remove(indice);
	}
	
	public void quita_archivo_remoto_abierto(String n) {
		int cantidad = this.archivos_remotos_abiertos.size();
		
		for (int i = 0; i < cantidad; i++) {
			String nombre = this.archivos_remotos_abiertos.get(i);
			if (nombre == n) {
				this.archivos_remotos_abiertos.remove(i);
			}
		}
	}
	
	public File obtiene_archivo_local_abierto(int indice) {
		try {
			return this.archivos_locales_abiertos.get(indice);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}
}
