package stub;
import java.io.Serializable;

public class RFSClientClose implements Serializable{
	private static final long serialVersionUID = 7627390217634423727L;
	private int fd;
	private String nombre;
	
	public RFSClientClose(int fd, String nombre) {
		this.fd = fd;
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getFd() {
		return fd;
	}

	public void setFd(int fd) {
		this.fd = fd;
	}
}
