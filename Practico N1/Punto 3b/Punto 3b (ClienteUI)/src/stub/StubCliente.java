package stub;

import java.io.*;

public class StubCliente {
	ObjectInputStream entrada;
	ObjectOutputStream salida;
	 
	public StubCliente(ObjectInputStream entrada, ObjectOutputStream salida) {
		this.entrada = entrada;
		this.salida = salida;
	}
	
	public int abrir_archivo(String nombre) {
		RFSClientOpen clnt_open = new RFSClientOpen(nombre);
		try {
			salida.writeObject(clnt_open);
			RFSServerOpen resultado = (RFSServerOpen) entrada.readObject();
			return resultado.getFd();
		} catch (Exception e) {
			return -1;
		}
	}
	
	public RespuestaLectura leer_archivo(int cant, int fd) {
		RFSClientRead clnt_read = new RFSClientRead(fd, cant);
		try {
			salida.writeObject(clnt_read);
			RFSServerRead resultado = (RFSServerRead) entrada.readObject();
			
			// Desempaqueta el objeto y devuelve la respuesta.
			RespuestaLectura respuesta = new RespuestaLectura(resultado.getFallo(), resultado.getTexto());
			return respuesta;
		} catch (Exception e) {
			RespuestaLectura respuesta = new RespuestaLectura(-1, "");
			return respuesta;
		}
	}
	
	public int escribir_archivo(int fd, String texto) {
		RFSClientWrite clnt_write = new RFSClientWrite(fd, texto);
		try {
			salida.writeObject(clnt_write);
			RFSServerWrite resultado = (RFSServerWrite) entrada.readObject();
			return resultado.getStatus();
		} catch (Exception e) {
			return -1;
		}
	}
	
	public int cerrar_archivo(int fd, String nombre) {
		RFSClientClose clnt_close = new RFSClientClose(fd, nombre);
		try {
			salida.writeObject(clnt_close);
			RFSServerClose resultado = (RFSServerClose) entrada.readObject();
			return resultado.getStatus();
		} catch (Exception e) {
			return -1;
		}
	}
	
	public class RespuestaLectura {
		private int estado;
		private String texto;
		
		public RespuestaLectura(int e, String t) {
			this.estado = e;
			this.texto = t;
		}

		public int getEstado() {
			return estado;
		}

		public void setEstado(int estado) {
			this.estado = estado;
		}

		public String getTexto() {
			return texto;
		}

		public void setTexto(String texto) {
			this.texto = texto;
		}
	}
}
