package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import javax.swing.JOptionPane;

import modelo.Cliente;
import stub.StubCliente;
import stub.StubCliente.RespuestaLectura;
import vista.Vista;
import vista.Vista.comboItem;

public class Controlador implements ActionListener{
	private static final int TOPE_BUFFER = 1024;
	private StubCliente stub;
	private Cliente modelo;
	private Vista vista;
	private Socket sock;
	private ObjectInputStream entrada;
	private ObjectOutputStream salida;

	public Controlador(Vista vista, Cliente modelo) {
		this.vista = vista;
		this.modelo = modelo;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String accion = e.getActionCommand();
		if (accion == "Conectar") {
			this.establecer_conexion();
		} else if (accion == "Desconectar") {
			this.romper_conexion();
		} else if (accion == "AbrirRemoto") {
			this.abrir_archivo_remoto();
		} else if (accion == "AbrirLocal") {
			this.abrir_archivo_local();
		} else if (accion == "Escribir") {
			this.escribir_archivo();
		} else if (accion == "Leer") {
			this.leer_archivo();
		} else if (accion == "CerrarRemoto") {
			this.cerrar_archivo_remoto();
		} else if (accion == "CerrarLocal") {
			this.cerrar_archivo_local();
		} else if (accion == "VolcarLocalARemoto") {
			this.volcar_datos_local_a_remoto();
		} else if (accion == "VolcarRemotoALocal") {
			this.volcar_datos_remoto_a_local();
		} else {
			this.vista.dialogo("Comando no reconocido!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void establecer_conexion() {
		String ip = this.vista.getIP();
		String puerto = this.vista.getPuerto();
		try {
			this.sock = new Socket(ip, Integer.parseInt(puerto));
			
			InputStream entrada_flj = this.sock.getInputStream();
			OutputStream salida_flj = this.sock.getOutputStream();
			
			this.entrada = new ObjectInputStream(entrada_flj);
			this.salida = new ObjectOutputStream(salida_flj);
			
			this.stub = new StubCliente(this.entrada, this.salida);
			
			this.vista.dialogo("Conectado!", "Conexión a host...", JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			this.vista.dialogo("Error de conexión!", "Conexión a host...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void romper_conexion() {
		try {
			// TODO cerrar si quedaron archivos abiertos.
			this.sock.close();
			this.vista.dialogo("Desconectado!", "Desconexión del host...", JOptionPane.INFORMATION_MESSAGE);
		} catch (IOException e) {
			this.vista.dialogo("Ocurrió un error cerrando el socket!", "Desconexión del host...", JOptionPane.ERROR_MESSAGE);
		} catch (NullPointerException e) {
			this.vista.dialogo("No existe una conexión activa!", "Desconexión del host...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void abrir_archivo_remoto() {
		String archivo = this.vista.getText_abrir_remoto();
		try {
			// Llamada al stub.
			int resultado = this.stub.abrir_archivo(archivo);
			
			if (resultado >= 0) {
				// Guarda el archivo abierto en el Cliente
				this.modelo.agrega_archivo_remoto_abierto(archivo);
				// Agrega el archivo abierto en la lista y en las combo
				// TODO this.vista.setList_archivos(this.vista.getList_archivos().add(archivo));
				this.vista.agregaItem_combo_archivo_remoto(resultado, archivo);
				this.vista.agregaItem_ComboCerrar_remoto(resultado, archivo);
				this.vista.setCombo_rw(resultado, archivo, "Remoto");
			} else if (resultado == -2) { 
				this.vista.dialogo("Archivo ya abierto!", "Abrir archivo remoto...", JOptionPane.ERROR_MESSAGE);
			} else {
				this.vista.dialogo("Ocurrió un error al intentar abrir el archivo remoto!", "Abrir archivo remoto...", JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception e) {
			this.vista.dialogo("Ocurrió una excepción al intentar abrir el archivo remoto!", "Abrir archivo remoto...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void abrir_archivo_local() {
		String nombre_archivo = this.vista.getText_abrir_local();
		try {
			File archivo = new File(nombre_archivo);
			BufferedReader lector = new BufferedReader(new FileReader(archivo));
			lector.close();
			int indice = this.modelo.getArchivos_locales_abiertos().size();
			this.modelo.agrega_archivo_local_abierto(indice, archivo);
			this.vista.agregaItem_combo_archivo_local(indice, nombre_archivo);
			this.vista.agregaItem_ComboCerrar_local(indice, nombre_archivo);
			this.vista.setCombo_rw(indice, nombre_archivo, "Local");
		} catch (FileNotFoundException e) {
			this.vista.dialogo("El archivo no existe!", "Abrir archivo local...", JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			this.vista.dialogo("Ocurrió un error al intentar abrir el archivo local!", "Abrir archivo local...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void escribir_archivo() {
		try {
			int resultado = 0;
			int fd = this.vista.getCombo_rw().getFd();
			String tipo = this.vista.getCombo_rw().getTipo();
			
			if (tipo == "Remoto") {
				try {
					// Llamada al stub.
					String texto = this.vista.obtiene_texto();
					
					int principio = 0;
					int fin = TOPE_BUFFER;
					int cantidad = texto.length();
					
					do {
						try {
							resultado = this.stub.escribir_archivo(fd, texto.substring(principio, fin));
							principio = fin;
							fin = fin + TOPE_BUFFER;
						} catch (IndexOutOfBoundsException e) {
							fin = cantidad;
							if (principio != fin) {
								resultado = this.stub.escribir_archivo(fd, texto.substring(principio, fin));
							}
							break;
						}
					} while (resultado != -1);
					
					if (resultado != 0) {
						this.vista.dialogo("Ocurrió un error al intentar escribir en el archivo remoto!", "Escribir archivo...", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					this.vista.dialogo("Ocurrió una excepción al intentar escribir en el archivo remoto!", "Escribir archivo...", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				try {
					String texto = this.vista.obtiene_texto();
					File archivo = this.modelo.obtiene_archivo_local_abierto(fd);
					BufferedWriter escritor = new BufferedWriter(new FileWriter(archivo));
					escritor.write(texto);
					escritor.close();
				} catch (Exception e) {
					this.vista.dialogo("Ocurrió una excepción al intentar escribir en el archivo local!", "Escribir archivo...", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (NullPointerException e) {
			this.vista.dialogo("Seleccione un archivo para escribir!", "Escribir archivo...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void leer_archivo () {
		try {
			int fd = this.vista.getCombo_rw().getFd();
			String tipo = this.vista.getCombo_rw().getTipo();
			
			if (tipo == "Remoto") {
				try {
					String texto = "";
					int estado = -1;
					
					do {
						// Llama al stub.
						Object resultado = this.stub.leer_archivo(TOPE_BUFFER, fd);
						estado = ((RespuestaLectura) resultado).getEstado();
						texto = texto + ((RespuestaLectura) resultado).getTexto();
					} while (estado == TOPE_BUFFER);
					
					if (estado != -1) {
						this.vista.actualiza_texto(texto);
					} else {
						this.vista.dialogo("Ocurrió un error al intentar leer en el archivo remoto!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					this.vista.dialogo("Ocurrió una excepción al intentar leer en el archivo remoto!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				try {
					File archivo = this.modelo.obtiene_archivo_local_abierto(fd);
					FileInputStream lector = new FileInputStream(archivo);
					
					String texto = "";
					int caracter = -1;
					
					while ((caracter = lector.read()) != -1) {
						texto = texto + (char) caracter;
					}
					this.vista.actualiza_texto(texto);
					lector.close();
				} catch (Exception e) {
					this.vista.dialogo("Ocurrió una excepción al intentar leer en el archivo local!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (NullPointerException e) {
			this.vista.dialogo("Seleccione un archivo para leer!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void cerrar_archivo_remoto() {
		try {
			int fd = this.vista.getCombo_cerrar_remoto().getFd();
			String nombre = this.vista.getCombo_cerrar_remoto().getNombre();
			
			try {
				// Llamada al stub.
				int resultado = this.stub.cerrar_archivo(fd, nombre);
				
				if (resultado != -1) {
					String tipo = this.vista.getCombo_archivo_remoto().getTipo();
					
					this.vista.borraItem_comboCerrar_remoto(resultado);
					this.vista.borraItem_combo_archivo_remoto(resultado);
					this.vista.borraItem_comboRW(fd, nombre, tipo);
					this.vista.actualiza_texto("");
				} else {
					this.vista.dialogo("Ocurrió un error al intentar cerrar en el archivo remoto!", "Cerrar archivo...", JOptionPane.ERROR_MESSAGE);
				}
			} catch (Exception e) {
				this.vista.dialogo("Ocurrió una excepción al intentar cerrar en el archivo remoto!", "Cerrar archivo...", JOptionPane.ERROR_MESSAGE);
			}
		} catch (NullPointerException e) {
			this.vista.dialogo("No hay archivos para cerrar!", "Cerrar archivo...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void cerrar_archivo_local() {
		try {
			comboItem item = this.vista.getCombo_cerrar_local();
			
			int fd = item.getFd();
			String nombre = item.getNombre();
			String tipo = item.getTipo();
			
			try {
				this.modelo.quita_archivo_local_abierto(fd);
				this.vista.borraItem_comboCerrar_local(fd);
				this.vista.borraItem_combo_archivo_local(fd);
				this.vista.borraItem_comboRW(fd, nombre, tipo);
				this.vista.actualiza_texto("");
			} catch (Exception e) {
				this.vista.dialogo("Ocurrió una excepción al intentar cerrar en el archivo remoto!", "Cerrar archivo...", JOptionPane.ERROR_MESSAGE);
			}
		} catch (NullPointerException e) {
			this.vista.dialogo("No hay archivos para cerrar!", "Cerrar archivo...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void volcar_datos_local_a_remoto() {
		try {
			int i;
			comboItem item_local = this.vista.getCombo_archivo_local();
			int fd_local = item_local.getFd();
			File archivo_local = this.modelo.obtiene_archivo_local_abierto(fd_local);
			FileInputStream lector = new FileInputStream(archivo_local);
			
			comboItem item_remoto = this.vista.getCombo_archivo_remoto();
			int fd_remoto = item_remoto.getFd();

			try {
				do {
					String texto = "";
					int caracter = -1;
					i = 0;
					
					while ((i < TOPE_BUFFER) && ((caracter = lector.read()) != -1)) {
						texto = texto + (char) caracter;
						i++;
					}
					
					int resultado = this.stub.escribir_archivo(fd_remoto, texto);
					
					if (resultado == -1) {
						this.vista.dialogo("Ocurrió un error en la escritura!", "Local a remoto...", JOptionPane.ERROR_MESSAGE);
					}
				} while (i == TOPE_BUFFER);
			} catch (Exception e) {
				lector.close();
				this.vista.dialogo("Ocurrió un error en la lectura/escritura!", "Local a remoto...", JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception e) {
			this.vista.dialogo("Ocurrió un error en el volcado!", "Local a remoto...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void volcar_datos_remoto_a_local() {
		try {
			comboItem item_local = this.vista.getCombo_archivo_local();
			int fd_local = item_local.getFd();
			File archivo_local = this.modelo.obtiene_archivo_local_abierto(fd_local);
			FileOutputStream escritor = new FileOutputStream(archivo_local, true);
			
			comboItem item_remoto = this.vista.getCombo_archivo_remoto();
			int fd_remoto = item_remoto.getFd();

			// Llama al stub.
			int estado = 0;
			String texto = "";
			do {
				Object resultado = this.stub.leer_archivo(TOPE_BUFFER, fd_remoto);
				estado = ((RespuestaLectura) resultado).getEstado();
				texto = ((RespuestaLectura) resultado).getTexto();
				if (estado != -1) {
					for (int i = 0; i < texto.length(); i++) {
						try {
							escritor.write((byte) texto.charAt(i));
						} catch (IOException e) {
							escritor.close();
							this.vista.dialogo("Ocurrió un error al intentar leer en el archivo remoto!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
							break;
						}	
					}
				} else {
					escritor.close();
					this.vista.dialogo("Ocurrió un error al intentar leer en el archivo remoto!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
					break;
				}
			} while (estado == TOPE_BUFFER);
		} catch (Exception e) {
			this.vista.dialogo("Ocurrió un error en el volcado!", "Remoto a local...", JOptionPane.ERROR_MESSAGE);
		}
	}
}
