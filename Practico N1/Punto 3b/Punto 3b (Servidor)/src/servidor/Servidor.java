package servidor;

import java.io.*;
import java.util.*;


public class Servidor {
	private ArrayList<Archivo> archivos_abiertos;
	
	public Servidor() {
		this.archivos_abiertos = new ArrayList<Archivo>();
	}
	
	public int abrir_archivo(String nombre) {
		try {
			File arch = new File(nombre);
			FileInputStream entrada = new FileInputStream(arch);
			FileOutputStream salida = new FileOutputStream(arch, true);
			Archivo archivo = new Archivo(nombre, arch, entrada, salida);
			this.archivos_abiertos.add(archivo);
			return 0;
		} catch (FileNotFoundException e) {
			return -1;
		}
	}
	
	public RespuestaLectura leer_archivo(int fd, int cantidad) {
		Archivo archivo = this.archivos_abiertos.get(fd);
		FileInputStream lector = archivo.getLector();
		String texto = "";
		int caracter = -1;
		int i = 0;
		
		try {
			while ((i < cantidad) && ((caracter = lector.read()) != -1)) {
				texto = texto + (char) caracter;
				i++;
			}
			if (caracter == -1) {
				lector.close();
				lector = new FileInputStream(archivo.getArchivo());
			}
			archivo.setLector(lector);
			RespuestaLectura respuesta = new RespuestaLectura(i, texto);
			return respuesta;
		} catch (IOException e) {
			RespuestaLectura respuesta = new RespuestaLectura(-1, "");
			return respuesta;
		}
	}
	
	public int escribir_archivo(int fd, String texto) {
		Archivo archivo = this.archivos_abiertos.get(fd);
		FileOutputStream escritor = archivo.getEscritor();
		
		for (int i = 0; i < texto.length(); i++) {
			try {
				escritor.write((byte) texto.charAt(i));
			} catch (IOException e) {
				return -1;
			}	
		}
		return 0;
	}
	
	public int cerrar_archivo(int fd) {
		Archivo archivo = this.archivos_abiertos.get(fd);
		try {
			archivo.lector.close();
			archivo.escritor.close();
			this.archivos_abiertos.remove(fd);
			return 0;
		} catch (Exception e) {
			return -1;
		}
	}
	
	public int existe_archivo(String nombre) {
		int cantidad = this.archivos_abiertos.size();
		
		for (int i = 0; i < cantidad; i++) {
			Archivo archivo = this.archivos_abiertos.get(i);
			String nombre_archivo = archivo.getNombre();
			
			if (nombre_archivo == nombre) {
				return i;
			}
		}
		return -1;
	}
		
	/* Clases internas */
	public class Archivo{
		private String nombre;
		private File archivo;
		private FileOutputStream escritor;
		private FileInputStream lector;
		
		public Archivo(String nombre, File archivo, FileInputStream entrada, FileOutputStream salida){
			this.nombre = nombre;
			this.archivo = new File(nombre);
			this.lector = entrada;
			this.escritor = salida;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public File getArchivo() {
			return archivo;
		}

		public void setArchivo(File archivo) {
			this.archivo = archivo;
		}

		public FileOutputStream getEscritor() {
			return escritor;
		}

		public void setEscritor(FileOutputStream escritor) {
			this.escritor = escritor;
		}

		public FileInputStream getLector() {
			return lector;
		}

		public void setLector(FileInputStream lector) {
			this.lector = lector;
		}
		
		public void escribir() {
		}
	}

	public class RespuestaLectura{
		private int cantidad;
		private String lectura;
		
		public RespuestaLectura(int cantidad, String lectura) {
			this.cantidad = cantidad;
			this.lectura = lectura;
		}

		public int getCantidad() {
			return cantidad;
		}

		public void setCantidad(int cantidad) {
			this.cantidad = cantidad;
		}

		public String getLectura() {
			return lectura;
		}

		public void setLectura(String lectura) {
			this.lectura = lectura;
		}
	}
}
