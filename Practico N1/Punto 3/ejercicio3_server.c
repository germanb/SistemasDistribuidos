#include "ejercicio3.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int * rfs_open_1_svc(open_record *argp, struct svc_req *rqstp) {
	static int  result;

	printf("Recibida una llamada open\n");
	result = open(argp->file_name, argp->flags);
	return &result;
}

file_data * rfs_read_1_svc(read_record *argp, struct svc_req *rqstp) {
	static file_data  result;
	int n;

	printf("Recibida una llamada read\n");
	result.file_data_val = (char *) malloc(argp->count);
	if (result.file_data_val == 0)
		result.file_data_len = 0;
	else
		result.file_data_len = read(argp->fd, result.file_data_val, argp->count);
	return &result;
}

file_data * rfs_write_1_svc(write_record *argp, struct svc_req *rqstp) {
	static file_data  result;
	static ssize_t exito;

	printf("Recibida una llamada write\n");
	result.file_data_val = argp->buffer;

	// exito = write(argp->fd, argp->buffer, argp->count);
	exito = dprintf(argp->fd, argp->buffer);

	if (exito == -1)
		result.file_data_len = 0;
	else
		result.file_data_len = 1;
	return &result;
}

int * rfs_close_1_svc(int *argp, struct svc_req *rqstp) {
	static int  result;

	printf("Recibida una llamada close\n");
	result = close(*argp);
	return &result;
}
