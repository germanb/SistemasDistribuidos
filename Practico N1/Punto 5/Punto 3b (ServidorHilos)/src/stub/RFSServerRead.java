package stub;

import java.io.Serializable;

public class RFSServerRead implements Serializable{
	private static final long serialVersionUID = 2782746235311955250L;
	private int fallo;
	private String texto;
	
	public RFSServerRead(int fallo, String texto) {
		this.setFallo(fallo);
		this.texto=texto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public int getFallo() {
		return fallo;
	}

	public void setFallo(int fallo) {
		this.fallo = fallo;
	}
}
