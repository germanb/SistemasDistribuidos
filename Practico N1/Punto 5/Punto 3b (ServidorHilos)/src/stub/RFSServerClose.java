package stub;

import java.io.Serializable;

public class RFSServerClose implements Serializable{
	private static final long serialVersionUID = -7312038283704368650L;	
	private int status;
	
	public RFSServerClose(int status) {
		this.status=status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
