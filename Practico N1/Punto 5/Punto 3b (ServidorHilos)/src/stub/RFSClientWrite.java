package stub;
import java.io.Serializable;

public class RFSClientWrite implements Serializable{
	private static final long serialVersionUID = -9161843536419587290L;
	private int fd;
	private String texto;
	
	public RFSClientWrite(int fd, String texto) {
		super();
		this.fd = fd;
		this.texto = texto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getFd() {
		return fd;
	}

	public void setFd(int fd) {
		this.fd = fd;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
}
