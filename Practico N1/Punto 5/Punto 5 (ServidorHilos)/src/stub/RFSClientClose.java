package stub;
import java.io.Serializable;

public class RFSClientClose implements Serializable{
	private static final long serialVersionUID = 7627390217634423727L;
	private int fd;
	
	public RFSClientClose(int fd) {
		super();
		this.fd = fd;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getFd() {
		return fd;
	}

	public void setFd(int fd) {
		this.fd = fd;
	}
}
