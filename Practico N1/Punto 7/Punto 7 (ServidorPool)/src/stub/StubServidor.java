package stub;


import java.io.*;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import servidor.Servidor;
import servidor.Servidor.RespuestaLectura;
import stub.RFSClientOpen;
import stub.RFSClientRead;
import stub.RFSClientWrite;
import stub.RFSClientClose;
import stub.RFSServerOpen;
import stub.RFSServerRead;
import stub.RFSServerWrite;
import stub.RFSServerClose;


public class StubServidor {
	public static void main(String[] args) {
		Servidor servidor = new Servidor();
		
		try {
			int puerto=8080;
			@SuppressWarnings("resource")
			ServerSocket escuchandoSocket = new ServerSocket(puerto);
			System.out.println("Escuchando en el puerto "+puerto);
			
			// Crea el pool
			ExecutorService ejecutor = Executors.newFixedThreadPool(3);
			
			while (true) {
				/*Esperando conexion*/
				System.out.println("Esperando conexiones...");
				Socket socketCliente = escuchandoSocket.accept();
				System.out.println("Cliente conectado!");
				
				// Agrega el cliente al pool.
				ejecutor.submit(new Tarea(socketCliente, servidor));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class Tarea implements Runnable{
	private Socket socket;
	private Servidor servidor;
	ObjectInputStream entrada;
	ObjectOutputStream salida;
	
	public Tarea(Socket s, Servidor srv){
		this.socket = s;
		this.servidor = srv;
	}

	@Override
	public void run() {
		try {
			salida = new ObjectOutputStream(socket.getOutputStream());
			entrada = new ObjectInputStream(socket.getInputStream());
			
			System.out.println("Esperando mensaje...");
			Object objeto; 
			try {
				while ((objeto = entrada.readObject()) != null) {

					if (objeto instanceof RFSClientOpen) {
						System.out.println("Recibido comando OPEN!");
						try {
							// Desempaqueta el nombre del archivo y lo abre.
							String nombre_archivo = ((RFSClientOpen) objeto).getNombre();
							RFSServerOpen open_srv = new RFSServerOpen(servidor.abrir_archivo(nombre_archivo));
							salida.writeObject(open_srv);
						} catch (FileNotFoundException e) {
							System.out.println("Error al ejecutar OPEN! (Archivo no encontrado)");
							// En caso de error devuelve -1 como descriptor de archivo.
							RFSServerOpen open_srv = new RFSServerOpen(-1);
							salida.writeObject(open_srv);
						} catch (Exception e) {
							System.out.println("Error al ejecutar OPEN!");
							// En caso de error devuelve -1 como descriptor de archivo.
							RFSServerOpen open_srv = new RFSServerOpen(-1);
							salida.writeObject(open_srv);
						}
					} else if (objeto instanceof RFSClientRead) {
						System.out.println("Recibido comando READ!");
						try {
							// Desempaqueta la petición.
							int fd = ((RFSClientRead) objeto).getFd();
							int cantidad = ((RFSClientRead) objeto).getCount();
							
							// Le pide al servidor que le devuelva la lectura del archivo pedido.
							RespuestaLectura lectura = servidor.leer_archivo(fd, cantidad);
							
							// Compone y envia la respuesta.
							int leido = lectura.getCantidad();
							String texto = lectura.getLectura();
							RFSServerRead read_srv = new RFSServerRead(leido, texto);
							salida.writeObject(read_srv);
						} catch (Exception e) {
							System.out.println("Error de lectura!");
							RFSServerRead read_srv = new RFSServerRead(-1, "");
							salida.writeObject(read_srv);
						}
					} else if (objeto instanceof RFSClientWrite) {
						System.out.println("Recibido comando WRITE!");
						
						// Desempaqueta el objeto.
						int fd = ((RFSClientWrite) objeto).getFd();
						String texto = ((RFSClientWrite) objeto).getTexto();
						
						try {
							int respuesta = servidor.escribir_archivo(fd, texto);
							RFSServerWrite write_srv = new RFSServerWrite(respuesta);
							salida.writeObject(write_srv);
						} catch (Exception e) {
							
						}
					} else if (objeto instanceof RFSClientClose) {
						System.out.println("Recibido comando CLOSE!");
						
						// Desempaqueta el objeto.
						int fd = ((RFSClientClose) objeto).getFd();
						int fallo = 0;
						
						try {
							// Cierra el archivo
							fallo = servidor.cerrar_archivo(fd);
							
							// Compone la respuesta y la envía.
							RFSServerClose close_srv = new RFSServerClose(fallo);
							salida.writeObject(close_srv);
						} catch (Exception e) {
							System.out.println("Ocurrió un error al cerrar!");
							RFSServerClose close_srv = new RFSServerClose(-1);
							salida.writeObject(close_srv);
						}
					} else {
						// TODO hacer algo aca.
					}
				}
			} catch (EOFException e) {
				System.out.println("El cliente cerró la conexión!");
				socket.close();
			} catch (SocketException e) {
				System.out.println("El cliente cerró la conexión!");
				socket.close();
			}
			socket.close();
		} catch (Exception e) {
			System.out.println("Error!");
		}
	}
}
