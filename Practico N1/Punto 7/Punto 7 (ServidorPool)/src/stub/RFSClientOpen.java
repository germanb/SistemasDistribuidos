package stub;
import java.io.Serializable;

public class RFSClientOpen implements Serializable{
	private static final long serialVersionUID = -1202779211607899041L;
	private String nombre;
	
	public RFSClientOpen(String nombre) {
		super();
		this.nombre = nombre;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
