package stub;

import java.io.Serializable;

public class RFSServerOpen implements Serializable{
	private static final long serialVersionUID = 390176011530222120L;
	private int fd;
	
	public RFSServerOpen(int fd) {
		this.fd=fd;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}	

	public int getFd() {
		return fd;
	}

	public void setFd(int fd) {
		this.fd = fd;
	}
}
