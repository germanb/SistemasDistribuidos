package stub;

import java.io.Serializable;

public class RFSServerWrite implements Serializable{
	private static final long serialVersionUID = 5375694533651338059L;
	private int status;
	
	public RFSServerWrite(int status) {
		this.status=status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}	
}

