package stub;
import java.io.Serializable;

public class RFSClientRead implements Serializable{
	private static final long serialVersionUID = 6340642140891405476L;
	private int fd;
	private int count;
	
	public RFSClientRead(int fd, int count) {
		super();
		this.fd = fd;
		this.count = count;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public int getFd() {
		return fd;
	}

	public void setFd(int fd) {
		this.fd = fd;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
