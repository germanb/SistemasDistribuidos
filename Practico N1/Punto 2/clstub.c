/*
 *	clstub.c
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

#include "prototipo.h"
#include "strustub.h"

char buffcom[ MAX_DATA ];
CLSVBUFF clsvbuff;

void error(char *msg)
{
    perror(msg);
    //exit(0);
}

/*
 *INTERFASE CON CAPA TRANSPORTE
*/

static int send_rcv( int s, CLSVBUFF *p, int opcode, int qty )
  {
  int qtyrec;
  p->opc = opcode;
  send_packet( s, p, qty + sizeof( OPC ) );
  qtyrec = receive_packet( s, p, sizeof( *p ) );
  return qtyrec - sizeof( OPC );
  }

//Emisión
int send_packet( int sockfd, const void *p, int qty )
    {
    int n;
    memcpy( buffcom, p, qty );
    n = write(sockfd,buffcom,qty);
    if (n < 0)
         error("ERROR al escribir socket");
    bzero(buffcom, MAX_DATA);
    return n;
    }

//Recepción
int receive_packet( int sockfd, void *p, int lim )
    {
    int n;
    n = read(sockfd,buffcom,lim);
    if (n < 0)
         error("ERROR al leer socket");
    else
	{
	memcpy( p, buffcom, n );
	bzero(buffcom, MAX_DATA);
	}
    return n;
  }

//INICIALIZACION
int sockcl_init(char *arg1, char *arg2)
    {
    int sockfd, portno, n;

    struct sockaddr_in serv_addr;
    struct hostent *server;

    portno = atoi(arg2);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");
    server = gethostbyname(arg1);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        return(-1);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,&serv_addr,sizeof(serv_addr)) < 0)
        error("ERROR connecting");

    //Una vez conectado, pruebo escribir y leer.
    n = write(sockfd,"Hola Server..",13);
    if (n < 0)
         error("ERROR writing to socket");
     n = read(sockfd,buffcom,MAX_DATA);
     if (n < 0)
	error("ERROR reading from socket");
     else
	printf("mensaje: %s\n",buffcom);
     bzero(buffcom, MAX_DATA);

   return sockfd;
}

/*
 *CAPA MIDDLEWARE (STUB)
 */

int rmtread( int s, void *data, int qty, int binicio, short flags, const char *pathname )
  {
  CLSV_RREAD *pc_read;
  SVCL_RREAD *ps_read;

  int status_read;

  pc_read = &clsvbuff.data.clsv_rread;
  ps_read = &clsvbuff.data.svcl_rread;
  pc_read->flags = flags;  //del open
  strcpy( pc_read->pathname, pathname ); //del open
  pc_read->qty = qty;
  pc_read->binicio = binicio;

  send_rcv( s, &clsvbuff, RREAD, sizeof( CLSV_RREAD ) );

  status_read = ps_read->status;
  if( status_read > 0 )
    memcpy( data, ps_read->data, status_read );

  return status_read;
  }

int rmtwrite( int s, const void *data, int qty, short flags, const char *pathname )
  {
  CLSV_RWRITE *pc_write;
  SVCL_RWRITE *ps_write;

  pc_write = &clsvbuff.data.clsv_rwrite;
  ps_write = &clsvbuff.data.svcl_rwrite;
  pc_write->flags = flags;  //del open
  strcpy( pc_write->pathname, pathname ); //del open
  pc_write->qty = qty;
  if( qty > 0 )
    memcpy( pc_write->data, data, qty );
  send_rcv( s, &clsvbuff, RWRITE, sizeof( CLSV_RWRITE ) );

  return ps_write->status;
  }
//.......................................................
