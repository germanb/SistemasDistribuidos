/*
 *	prototipo.h
 */
 
typedef int RD;

/*
 *	Funciones remotas
 */



int rmtread( int s, void *data, int qty, int binicio, short flags, const char *pathname );

int rmtwrite( int s, const void *data, int qty, short flags, const char *pathname );

/*
 *	Funciones de Stubs
 */

int send_packet( int sockfd, const void *p, int qty );

int receive_packet( int sockfd, void *p, int lim );
