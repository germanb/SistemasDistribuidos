/*
 *  server.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "prototipo.h"

int rmtread(int s, void *data, int qty, int binicio, short flags, const char *pathname){
	//open
	RD myfile;
	int cant_leida;
	myfile = open( pathname, flags, 0644 );
	//read
	lseek(myfile, binicio, SEEK_SET);
	cant_leida = read(myfile, data, qty );
	//close
	close(myfile);
	return cant_leida;
}

int rmtwrite(int s, const void *data, int qty, short flags, const char *pathname){
	//open
	RD myfile;
	int estado;
	int cant_escrita;
	myfile = open( pathname, flags, 0644 );
	if (myfile<0) {
		return -1;
	}
	//read
	int val;
 	cant_escrita = write( myfile, data, qty );
	val = fsync(myfile);
	//close
	estado = close(myfile);
	if (estado<0) {
		return -2;
	}
	return cant_escrita;
}
