
/*
 *	strustub.h
 *	Estructuras del Middleware
 */
//executors newThreadPool
typedef int OPC;

#define MAX_NAME	50
#define MAX_DATA	512

enum { RREAD, RWRITE  }; //MAX_OPCODES

/*	Client -> server	*/


typedef struct
  {
  int qty;
  int binicio;
  short flags;
  char pathname[ MAX_NAME ];
  } CLSV_RREAD;

typedef struct
  {
  int qty;
  char data[ MAX_DATA ];
  short flags;
  char pathname[ MAX_NAME ];
  } CLSV_RWRITE;

/*	Server -> client	*/

typedef struct
  {
  int status;
  char data[ MAX_DATA ];
  } SVCL_RREAD;

typedef struct
  {
  int status;
  } SVCL_RWRITE;


typedef union
  {
  CLSV_RREAD	 clsv_rread;
  CLSV_RWRITE	 clsv_rwrite;
  SVCL_RREAD	 svcl_rread;
  SVCL_RWRITE	 svcl_rwrite;
  } DATA;

  typedef struct
    {
    OPC opc;
    DATA data;
    } CLSVBUFF;
