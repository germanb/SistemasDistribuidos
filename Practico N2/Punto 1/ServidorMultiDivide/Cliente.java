/*
 * Cliente.java
 *
 * Ejemplo de muy simple de rmi
 */

import java.rmi.Naming;                    /* lookup         */
import java.rmi.registry.Registry;         /* REGISTRY_PORT  */

import java.net.MalformedURLException;     /* Exceptions...  */
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

/**
 * Ejemplo de cliente rmi nocivo, para aprovecharse de un servidor sin
 * SecurityManager.
 * @author  Javier Abellon
 */
public class Cliente {

    /** Crea nueva instancia de Cliente */
    public Cliente(String alfa) 
    {
        try
        {
            // Lugar en el que esta el objeto remoto.
            // Debe reemplazarse "localhost" por el nombre o ip donde
            // esta corriendo "rmiregistry".
            // Naming.lookup() obtiene el objeto remoto
            String rname = "//" + alfa + ":" + Registry.REGISTRY_PORT + "/ObjetoRemoto";
            InterfaceRemota objetoRemoto = (InterfaceRemota)Naming.lookup (rname);

            int opcion = -1;

            while (opcion != 0){
                System.out.println("Menu:");
                System.out.println("1. Realizar una multiplicacion remota");
                System.out.println("2. Realizar una division remota");
                System.out.println("0. Salir");
                Scanner sc = new Scanner(System.in);
                System.out.print(">>> ");
                while (!sc.hasNextInt()) sc.next();
                    opcion = sc.nextInt();

                if (opcion != 0){
                    int numero1 = 0;
                    int numero2 = 0;
                    int resultado = 0;

                    System.out.print("Ingrese el primer numero: ");
                    while (!sc.hasNextInt()) sc.next();
                        numero1 = sc.nextInt();
                        
                    System.out.print("Ingrese el segundo numero ");
                    while (!sc.hasNextInt()) sc.next();
                        numero2 = sc.nextInt();

                    if (opcion == 1){  // Multiplicacion
                        resultado = objetoRemoto.multiplica(numero1, numero2);
                    } else if (opcion == 2){  // Division
                        resultado = objetoRemoto.divide(numero1, numero2);
                    } else {  // Opcion no reconocida
                        resultado = -1;
                    }
                    System.out.println("El resultado es: "+resultado);
                    System.out.println("");
                }
            }
        } catch (MalformedURLException e) {
            System.out.println("MalformedUrl");
	        e.printStackTrace();
        } catch (RemoteException e) {
            System.out.println("RemoteException");
            e.printStackTrace();
        } catch (NotBoundException e) {
            System.out.println("NotBoundException");
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            new Cliente(args[0]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Uso: java Cliente <ip>");
        }
        
    }
    
}
