/*
 * Cliente.java
 *
 * Ejemplo de muy simple de rmi
 */

import java.rmi.Naming;                    /* lookup         */
import java.rmi.registry.Registry;         /* REGISTRY_PORT  */
import java.rmi.server.RMISocketFactory;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.MalformedURLException;
import java.rmi.UnmarshalException;     /* Exceptions...  */
import java.net.SocketTimeoutException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

/**
 * Ejemplo de cliente rmi nocivo, para aprovecharse de un servidor sin
 * SecurityManager.
 * @author  Javier Abellon
 */
public class Cliente {

    /** Crea nueva instancia de Cliente */
    public Cliente(String alfa) 
    {
        try
        {
            // Se crea el factory, dandole un tiempo de 10 segundos de tolerancia.
            try{
                RMISocketFactory.setSocketFactory(new MiSocketFactory(10));
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Se crea la conexión.
            String rname = "//" + alfa + ":" + Registry.REGISTRY_PORT + "/ObjetoRemoto";
            InterfaceRemota objetoRemoto = (InterfaceRemota)Naming.lookup (rname);

            int opcion = -1;

            while (opcion != 0){
                System.out.println("Menu:");
                System.out.println("1. Realizar una suma remota");
                System.out.println("2. Realizar una resta remota");
                System.out.println("0. Salir");
                Scanner sc = new Scanner(System.in);
                System.out.print(">>> ");
                while (!sc.hasNextInt()) sc.next();
                    opcion = sc.nextInt();

                if (opcion != 0){
                    int numero1 = 0;
                    int numero2 = 0;
                    int resultado = 0;

                    System.out.print("Ingrese el primer numero: ");
                    while (!sc.hasNextInt()) sc.next();
                        numero1 = sc.nextInt();
                        
                    System.out.print("Ingrese el segundo numero ");
                    while (!sc.hasNextInt()) sc.next();
                        numero2 = sc.nextInt();

                    if (opcion == 1){  // Suma
                        try{
                            resultado = objetoRemoto.suma(numero1, numero2);
                        } catch (UnmarshalException e) {
                            System.out.println("Timeout!");
                        }
                    } else if (opcion == 2){  // Resta
                        try {
                            resultado = objetoRemoto.resta(numero1, numero2);
                        } catch (UnmarshalException e) {
                            System.out.println("Timeout!");
                        }
                    } else {  // Opcion no reconocida
                        resultado = -1;
                    }
                    System.out.println("El resultado es: "+resultado);
                    System.out.println("");
                }
            }
        } catch (MalformedURLException e) {
            System.out.println("MalformedUrl");
	        e.printStackTrace();
        } catch (RemoteException e) {
            System.out.println("RemoteException");
            e.printStackTrace();
        } catch (NotBoundException e) {
            System.out.println("NotBoundException");
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            new Cliente(args[0]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Uso: java Cliente <ip>");
        }
        
    }
    
}
