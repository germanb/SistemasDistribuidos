import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.server.RMISocketFactory;

class MiSocketFactory extends RMISocketFactory {
    private int timeout;
    
    public MiSocketFactory(int timeout) {
        this.timeout = timeout;
    }
    
    public Socket createSocket(String host, int port) throws IOException {
        Socket ret = getDefaultSocketFactory().createSocket(host, port);
        ret.setSoTimeout(timeout * 1000);
        return ret;
    }
    
    public ServerSocket createServerSocket(int port) throws IOException {
        return getDefaultSocketFactory().createServerSocket(port);
    }
} 