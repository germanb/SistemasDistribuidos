/*
 * ObjetoRemoto.java
 *
 * Created on 27 de abril de 2004, 21:18
 */

//package chuidiang.ejemplos.rmi.suma;

//import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.TimeUnit;

/**
 * @author  Javier Abellon
 */
public class ObjetoRemoto extends UnicastRemoteObject implements InterfaceRemota
{
    /**
     * Construye una instancia de ObjetoRemoto
     * @throws RemoteException
     */
    protected ObjetoRemoto () throws RemoteException
    {
        super();
    }

    /**
     * Obtiene la suma de los sumandos que le pasan y la devuelve.
     */
    public int suma(int a, int b, String pid) 
    {
        System.out.println ("Sumando " + a + " + " + b +"...");
        System.out.println("Comenzando la cuenta regresiva para el cliente " + pid);
        int cuenta = 5;
        try {
            while (cuenta > 0){
                System.out.println(cuenta);
                TimeUnit.SECONDS.sleep(1);
                cuenta = cuenta - 1;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Finalizó " + pid);
        return a+b;
    }

    public int resta(int a, int b){
        System.out.println("Restando "+ a + " - " + b + "...");
        try{
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return a-b;
    }
}
