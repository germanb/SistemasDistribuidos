/*
 * InterfaceRemota.java
 *
 * Created on 27 de abril de 2004, 21:17
 */

//package chuidiang.ejemplos.rmi.suma;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface remota con los metodos que se pueden llamar en remoto
 * @author  Javier Abellon
 */
public interface InterfaceRemota extends Remote {
    public int suma(int a, int b, String pid) throws RemoteException; 
    public int resta(int a, int b) throws RemoteException;
}
