import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
	private Cliente modelo;
	private Vista vista;

	public Controlador(Vista vista, Cliente modelo) {
		this.vista = vista;
		this.modelo = modelo;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String accion = e.getActionCommand();
		if (accion == "Actualizar") {
			this.actualizar_hora();
		}else {
			this.vista.dialogo("Comando no reconocido!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void mostrarHora(int horas, int minutos, int segundos){
		this.vista.setHoraMostrada(horas, minutos, segundos);
	}

	public void  actualizar_hora(){
		this.modelo.actualizarHora(this.modelo.getStub());
	}

}
