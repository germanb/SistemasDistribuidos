import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
        
public class ServidorHora implements Reloj {
        
    public ServidorHora() {}

    public long getHora() {
        return System.currentTimeMillis(); //hora en milisegundos
    }
        
    public static void main(String args[]) {
        
        try {
            ServidorHora obj = new ServidorHora();
            Reloj stub = (Reloj) UnicastRemoteObject.exportObject(obj, 0);
            // Liga el stub del objeto remoto en el registry
            Registry registry = LocateRegistry.getRegistry();
            registry.bind("Reloj", stub);
            //while(true){
            //    System.out.println(obj.getHora());
            //}
            System.err.println("Servidor listo");
        } catch (RemoteException e){
            System.out.println("Error remotox");
        } catch (Exception e){
            System.err.println("Excepcion en el servidor: " + e.toString());
            e.printStackTrace();
        }
    }
}