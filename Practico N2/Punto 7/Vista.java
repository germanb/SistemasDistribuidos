import java.awt.Frame;

import javax.swing.JFrame;
import java.awt.EventQueue;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

public class Vista extends Frame {

	private JFrame frmReloj;
	private JLabel lblHora;
	private Cliente modelo;
	private Controlador controlador;
	private static final long serialVersionUID = 6340642140891405476L;
	

    public Vista(String host) { 
		this.modelo = new Cliente(host);
		this.controlador = new Controlador(this, this.modelo);
		this.modelo.setControlador(this.controlador);
		initialize();
    }

	public void dialogo(String texto, String titulo, int flag) {
		JOptionPane.showMessageDialog(new JFrame(), texto, titulo, flag);
	}
	
    public void initialize(){
        //Frame
        this.frmReloj = new JFrame();
        frmReloj.setTitle("Reloj - RMI");
		frmReloj.setBounds(100, 100, 627, 180);
		frmReloj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmReloj.getContentPane().setLayout(null);
        
        JPanel panelTitulo = new JPanel();
		panelTitulo.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		panelTitulo.setBounds(10, 11, 600, 40);
		frmReloj.getContentPane().add(panelTitulo);
        panelTitulo.setLayout(null);
        		
    	JLabel lblTitulo = new JLabel("Reloj Digital");
		lblTitulo.setBounds(270, 12, 200, 14);
		panelTitulo.add(lblTitulo);
    
        JPanel panelHora = new JPanel();
        panelHora.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
        panelHora.setBounds(10, 70, 600, 100);
		frmReloj.getContentPane().add(panelHora);
        panelHora.setLayout(null);

    	JLabel lblTextoHora = new JLabel("Reloj Digital");
		lblTextoHora.setBounds(10, 11, 100, 14);
		panelHora.add(lblTextoHora);

        this.lblHora = new JLabel("00:00:00");
		lblHora.setBounds(150, 11, 100, 14);
		panelHora.add(lblHora);

        JButton btnActualizar = new JButton("Actualizar Hora");
		btnActualizar.addActionListener(this.controlador);
		btnActualizar.setActionCommand("Actualizar");
		btnActualizar.setBounds(420, 11, 150, 14);
		panelHora.add(btnActualizar);
	}
	
	public void setHoraMostrada(int horas, int minutos, int segundos){
		this.lblHora.setText(horas + ":" + minutos + ":" + segundos);
	}

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					String host = (args.length < 1) ? null : args[0];							
					Vista window = new Vista(host);
					window.frmReloj.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
    }
}