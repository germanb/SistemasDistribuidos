import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.lang.Boolean;
import java.util.Calendar;

public class Cliente extends Thread {

    public static int segundosDif = 120;
    public static int granularidadEnMilisDelta = 500;
    private Calendar calendario;
    private Controlador controlador;
    private int delta;
    private Boolean cambio;
    private int veces;
    private Reloj stub;

    public Cliente(String host) {
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            this.stub = (Reloj) registry.lookup("Reloj");
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
        this.calendario = Calendar.getInstance();
        this.calendario.add(Calendar.SECOND, segundosDif);
        this.setDelta(0);
        this.setCambio(false);
        this.setVeces(0);
        this.start();        
    }

    public int getHoraCalendario(){
        return this.calendario.get(Calendar.HOUR);
    }

    public int getMinutosCalendario(){
        return this.calendario.get(Calendar.MINUTE);
    }

    public int getSegundosCalendario(){
        return this.calendario.get(Calendar.SECOND);
    }

    private int getVeces(){
        return this.veces;
    }

    private void setVeces(int cant){
        this.veces = cant;
    }

    private Boolean getCambio(){
        return this.cambio;
    }

    private void setCambio(Boolean valorDeVerdad){
        this.cambio = valorDeVerdad;
    }

    private int getDelta(){
        return this.delta;
    }

    private void setDelta(int valor){
        this.delta = valor;
    }

    public Reloj getStub(){
        return this.stub;
    }

    public void setControlador(Controlador controler){
        this.controlador = controler;
    }

    public void run(){
        try{
            while(true){
                //Si hubo un cambio en el delta, va a contar cuantas veces tiene que aplicarse ese delta sobre el segundo antes de
                //volver a la normalidad
                if(this.getCambio()){
                    if(this.veces<1){
                        this.setDelta(0);
                        this.setCambio(false);
                    }else{
                        this.setVeces(this.getVeces()-1);
                    }
                }
                //Funcionamiento de reloj
                Thread.sleep(1000+this.getDelta()); //cada 1 segundo + delta, el reloj aumenta 1 segundo.
                this.calendario.add(Calendar.SECOND, 1);
                this.controlador.mostrarHora(this.getHoraCalendario(), this.getMinutosCalendario(), this.getSegundosCalendario());
                //System.out.println("La diferencia en milisegundos es: " + diferencia);
                //System.out.println("Cantidad de segundos con delta restantes: " + this.getVeces());
            }
        }catch(Exception e){
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }        
    }

    private long algoritmoCristian(Reloj stub){
        try{
            long segundaObtenida = 0;
            long sumatoriaRTT = 0;
            long primeraObtenida = stub.getHora();
            for(int i=0; i<5;i++){
                segundaObtenida = stub.getHora();
                sumatoriaRTT = sumatoriaRTT + (segundaObtenida - primeraObtenida);
                primeraObtenida = segundaObtenida;
            }
            return primeraObtenida + ((sumatoriaRTT / 5) / 2); //Ultima hora obtenida + (promedio RTT / 2)
        }catch(Exception e){
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
            return -1;
        }
    }

    public void actualizarHora(Reloj stub){
                long horaCalculada = algoritmoCristian(stub); //Realiza el algoritmo de Cristian y obtiene la hora calculada del server.
                long horaCliente = this.calendario.getTimeInMillis(); //Obtiene hora del cliente para comparar con la calculada
                long diferencia = horaCliente - horaCalculada; //Si es positivo, el reloj esta adelantado. Si es negativo, esta atrasado.
                System.out.println("La diferencia en milisegundos es: " + diferencia);

                if(diferencia!=0){ //si el reloj no esta exactamente sicronizado a nivel milisegundos
                    if (diferencia > 0){ //reloj adelantado
                        this.setDelta(granularidadEnMilisDelta); //aumenta 100 milisegundos el tiempo entre cada segundo del reloj
                    }else{ //reloj atrasado
                        this.setDelta(-granularidadEnMilisDelta); //reduce 100 milisegundos el tiempo entre cada segundo del reloj
                    }
                    this.setVeces((int)(Math.abs(diferencia) / granularidadEnMilisDelta)); //10 veces por segundo de diferencia
                    this.setCambio(true);
                }
                int a = 1000 + this.getDelta();
                System.out.println("Trabajando con: " + a);
            //while(this.getVeces()>0){
            //    try{
            //        Thread.sleep(1000);                    
            //    }catch(Exception e){
            //        System.err.println("Fallo el sleep");
            //    }
                //System.out.println("La diferencia en milisegundos es: " + diferencia);                
            //    System.out.println("Cantidad de segundos con delta restantes: " + this.getVeces());
            //}
        }

}