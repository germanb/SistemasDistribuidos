import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Reloj extends Remote {
    long getHora() throws RemoteException;
}