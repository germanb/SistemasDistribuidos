/*
 * ObjetoRemoto.java
 *
 * Created on 27 de abril de 2004, 21:18
 */

//package chuidiang.ejemplos.rmi.suma;

//import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.io.*;
import java.util.*;

/**
 * @author  Javier Abellon
 */
public class ObjetoRemoto extends UnicastRemoteObject implements InterfaceRemota
{
	private static final long serialVersionUID = 1L;
	private ArrayList<Archivo> archivos_abiertos;

    /**
     * Construye una instancia de ObjetoRemoto
     * @throws RemoteException
     */
    protected ObjetoRemoto () throws RemoteException
    {
        super();
        this.archivos_abiertos = new ArrayList<Archivo>();
    }

    public int RFSOpen(String nombre) throws RemoteException{
        try {
			File arch = new File(nombre);
			FileInputStream entrada = new FileInputStream(arch);
			FileOutputStream salida = new FileOutputStream(arch, true);
            Archivo archivo = new Archivo(nombre, arch, entrada, salida);
			this.archivos_abiertos.add(archivo);
			return 0;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return -1;
		}
    } 

    public RespuestaLectura RFSRead(int fd, int cantidad) throws RemoteException {
		Archivo archivo = this.archivos_abiertos.get(fd);
		FileInputStream lector = archivo.getLector();
		String texto = "";
		int caracter = -1;
		int i = 0;
		
		try {
			while ((i < cantidad) && ((caracter = lector.read()) != -1)) {
				texto = texto + (char) caracter;
				i++;
			}
			if (caracter == -1) {
				lector.close();
				lector = new FileInputStream(archivo.getArchivo());
			}
			archivo.setLector(lector);
			RespuestaLectura respuesta = new RespuestaLectura(i, texto);
			return respuesta;
		} catch (IOException e) {
			RespuestaLectura respuesta = new RespuestaLectura(-1, "");
			e.printStackTrace();
			return respuesta;
		}
	}
    
    public int RFSWrite(int fd, String texto) throws RemoteException {
        Archivo archivo = this.archivos_abiertos.get(fd);
		FileOutputStream escritor = archivo.getEscritor();
		
		for (int i = 0; i < texto.length(); i++) {
			try {
				escritor.write((byte) texto.charAt(i));
			} catch (IOException e) {
				e.printStackTrace();
				return -1;
			}	
		}
		return 0;
    } 
    
    public int RFSClose(int fd) throws RemoteException {
        Archivo archivo = this.archivos_abiertos.get(fd);
		try {
			int resul_lector = archivo.cerrar_lector();
			int resul_escritor = archivo.cerrar_escritor();
			
			if ((resul_lector == 0) && (resul_escritor == 0)) {
				this.archivos_abiertos.remove(fd);
				return 0;
			} else {
				return -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
    }

    private int existe_archivo(String nombre) {
		int cantidad = this.archivos_abiertos.size();
		
		for (int i = 0; i < cantidad; i++) {
			Archivo archivo = this.archivos_abiertos.get(i);
			String nombre_archivo = archivo.getNombre();
			
			if (nombre_archivo == nombre) {
				return i;
			}
		}
		return -1;
	}
}
