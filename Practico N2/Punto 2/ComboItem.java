public class ComboItem {
    private int fd;
    private String nombre;
    private String tipo;
    
    public ComboItem(int fd, String nombre, String tipo) {
        this.fd = fd;
        this.nombre = nombre;
        this.tipo = tipo;
    }
    
    @Override
    public String toString() {
        return this.nombre + "(" + this.tipo + ")";
    }

    public int getFd() {
        return fd;
    }

    public void setFd(int fd) {
        this.fd = fd;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getTipo() {
        return tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}