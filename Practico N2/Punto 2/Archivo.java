import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Archivo{
    private String nombre;
    private File archivo;
    private FileOutputStream escritor;
    private FileInputStream lector;
    
    public Archivo(String nombre, File archivo, FileInputStream entrada, FileOutputStream salida){
        this.nombre = nombre;
        this.archivo = new File(nombre);
        this.lector = entrada;
        this.escritor = salida;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public File getArchivo() {
        return archivo;
    }

    public void setArchivo(File archivo) {
        this.archivo = archivo;
    }

    public FileOutputStream getEscritor() {
        return escritor;
    }

    public void setEscritor(FileOutputStream escritor) {
        this.escritor = escritor;
    }

    public FileInputStream getLector() {
        return lector;
    }

    public void setLector(FileInputStream lector) {
        this.lector = lector;
    }
    
    public int cerrar_lector(){
        try{
            this.lector.close();
            return 0;
        } catch (IOException e){
            return 1;
        }
    }

    public int cerrar_escritor(){
        try{
            this.escritor.close();
            return 0;
        } catch (IOException e){
            return 1;
        }
    }
}