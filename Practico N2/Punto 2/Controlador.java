import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import javax.swing.JOptionPane;

import java.rmi.Naming;                    /* lookup         */
import java.rmi.registry.Registry;         /* REGISTRY_PORT  */

import java.net.MalformedURLException;     /* Exceptions...  */
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;



public class Controlador implements ActionListener{
	private static final int TOPE_BUFFER = 1024;
	private Cliente modelo;
	private Vista vista;
	private ObjectInputStream entrada;
	private ObjectOutputStream salida;
	private InterfaceRemota objetoRemoto;

	public Controlador(Vista vista, Cliente modelo) {
		this.vista = vista;
		this.modelo = modelo;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String accion = e.getActionCommand();
		if (accion == "Conectar") {
			this.establecer_conexion();
		} else if (accion == "Desconectar") {
			this.romper_conexion();
		} else if (accion == "AbrirRemoto") {
			this.abrir_archivo_remoto();
		} else if (accion == "AbrirLocal") {
			this.abrir_archivo_local();
		} else if (accion == "Escribir") {
			this.escribir_archivo();
		} else if (accion == "Leer") {
			this.leer_archivo();
		} else if (accion == "CerrarRemoto") {
			this.cerrar_archivo_remoto();
		} else if (accion == "CerrarLocal") {
			this.cerrar_archivo_local();
		} else if (accion == "VolcarLocalARemoto") {
			this.volcar_datos_local_a_remoto();
		} else if (accion == "VolcarRemotoALocal") {
			this.volcar_datos_remoto_a_local();
		} else {
			this.vista.dialogo("Comando no reconocido!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void establecer_conexion() {
		String ip = this.vista.getIP();
		try{
			String rname = "//" + ip + ":" + Registry.REGISTRY_PORT + "/ObjetoRemoto";
			this.objetoRemoto = (InterfaceRemota)Naming.lookup (rname);
			this.vista.dialogo("Conectado!", "Conexion a host...", JOptionPane.INFORMATION_MESSAGE);
		} catch (MalformedURLException e) {
			System.out.println("MalformedUrl");
			this.vista.dialogo("Error de conexion!", "Conexion a host...", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (RemoteException e) {
			System.out.println("RemoteException");
			this.vista.dialogo("Error de conexion!", "Conexion a host...", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (NotBoundException e) {
			System.out.println("NotBoundException");
			this.vista.dialogo("Error de conexion!", "Conexion a host...", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void romper_conexion() {
		// TODO Ver que se hace para desconectarse
		this.vista.dialogo("Desconectado!", "Desconexion del host...", JOptionPane.INFORMATION_MESSAGE);
	}
	
	private void abrir_archivo_remoto() {
		String archivo = this.vista.getText_abrir_remoto();
		try {
			// Llamada al objeto remoto.
			int resultado = this.objetoRemoto.RFSOpen(archivo);
			
			if (resultado >= 0) {
				// Guarda el archivo abierto en el Cliente
				this.modelo.agrega_archivo_remoto_abierto(archivo);
				// Agrega el archivo abierto en la lista y en las combo
				// TODO this.vista.setList_archivos(this.vista.getList_archivos().add(archivo));
				this.vista.agregaItem_combo_archivo_remoto(resultado, archivo);
				this.vista.agregaItem_ComboCerrar_remoto(resultado, archivo);
				this.vista.setCombo_rw(resultado, archivo, "Remoto");
			} else if (resultado == -2) { 
				this.vista.dialogo("Archivo ya abierto!", "Abrir archivo remoto...", JOptionPane.ERROR_MESSAGE);
			} else {
				this.vista.dialogo("Ocurrio un error al intentar abrir el archivo remoto!", "Abrir archivo remoto...", JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception e) {
			this.vista.dialogo("Ocurrio una excepcion al intentar abrir el archivo remoto!", "Abrir archivo remoto...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void abrir_archivo_local() {
		String nombre_archivo = this.vista.getText_abrir_local();
		try {
			File archivo = new File(nombre_archivo);
			BufferedReader lector = new BufferedReader(new FileReader(archivo));
			lector.close();
			int indice = this.modelo.getArchivos_locales_abiertos().size();
			this.modelo.agrega_archivo_local_abierto(indice, archivo);
			this.vista.agregaItem_combo_archivo_local(indice, nombre_archivo);
			this.vista.agregaItem_ComboCerrar_local(indice, nombre_archivo);
			this.vista.setCombo_rw(indice, nombre_archivo, "Local");
		} catch (FileNotFoundException e) {
			this.vista.dialogo("El archivo no existe!", "Abrir archivo local...", JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			this.vista.dialogo("Ocurrio un error al intentar abrir el archivo local!", "Abrir archivo local...", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void escribir_archivo() {
		try {
			int resultado = 0;
			int fd = this.vista.getCombo_rw().getFd();
			String tipo = this.vista.getCombo_rw().getTipo();
			
			if (tipo == "Remoto") {
				try {
					// Llamada al objeto remoto.
					String texto = this.vista.obtiene_texto();
					
					int principio = 0;
					int fin = TOPE_BUFFER;
					int cantidad = texto.length();
					
					do {
						try {
							resultado = this.objetoRemoto.RFSWrite(fd, texto.substring(principio, fin));
							principio = fin;
							fin = fin + TOPE_BUFFER;
						} catch (IndexOutOfBoundsException e) {
							fin = cantidad;
							if (principio != fin) {
								resultado = this.objetoRemoto.RFSWrite(fd, texto.substring(principio, fin));
							}
							break;
						}
					} while (resultado != -1);

					if (resultado != 0) {
						this.vista.dialogo("Ocurrio un error al intentar escribir en el archivo remoto!", "Escribir archivo...", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					this.vista.dialogo("Ocurrio una excepcion al intentar escribir en el archivo remoto!", "Escribir archivo...", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				try {
					String texto = this.vista.obtiene_texto();
					File archivo = this.modelo.obtiene_archivo_local_abierto(fd);
					BufferedWriter escritor = new BufferedWriter(new FileWriter(archivo));
					escritor.write(texto);
					escritor.close();
				} catch (Exception e) {
					this.vista.dialogo("Ocurrio una excepcion al intentar escribir en el archivo local!", "Escribir archivo...", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (NullPointerException e) {
			this.vista.dialogo("Seleccione un archivo para escribir!", "Escribir archivo...", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void leer_archivo () {
		try {
			int fd = this.vista.getCombo_rw().getFd();
			String tipo = this.vista.getCombo_rw().getTipo();
			
			if (tipo == "Remoto") {
				try {
					String texto = "";
					int estado = -1;

					do {
						// Llama al objeto remoto.
						Object resultado = this.objetoRemoto.RFSRead(fd, TOPE_BUFFER);
						estado = ((RespuestaLectura) resultado).getCantidad();
						texto = texto + ((RespuestaLectura) resultado).getLectura();
					} while (estado == TOPE_BUFFER);
					
					if (estado != -1) {
						this.vista.actualiza_texto(texto);
					} else {
						this.vista.dialogo("Ocurrio un error al intentar leer en el archivo remoto!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					this.vista.dialogo("Ocurrio una excepcion al intentar leer en el archivo remoto!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			} else {
				try {
					File archivo = this.modelo.obtiene_archivo_local_abierto(fd);
					FileInputStream lector = new FileInputStream(archivo);

					String texto = "";
					int caracter = -1;

					while ((caracter = lector.read()) != -1) {
						texto = texto + (char) caracter;
					}
					this.vista.actualiza_texto(texto);
					lector.close();
				} catch (Exception e) {
					this.vista.dialogo("Ocurrio una excepcion al intentar leer en el archivo local!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (NullPointerException e) {
			this.vista.dialogo("Seleccione un archivo para leer!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void cerrar_archivo_remoto() {
		try {
			int fd = this.vista.getCombo_cerrar_remoto().getFd();
			String nombre = this.vista.getCombo_cerrar_remoto().getNombre();

			try {
				// Llamada al objeto remoto.
				int resultado = this.objetoRemoto.RFSClose(fd);

				if (resultado != -1) {
					String tipo = this.vista.getCombo_archivo_remoto().getTipo();
					
					this.vista.borraItem_comboCerrar_remoto(resultado);
					this.vista.borraItem_combo_archivo_remoto(resultado);
					this.vista.borraItem_comboRW(fd, nombre, tipo);
					this.vista.actualiza_texto("");
				} else {
					this.vista.dialogo("Ocurrio un error al intentar cerrar en el archivo remoto!", "Cerrar archivo...", JOptionPane.ERROR_MESSAGE);
				}
			} catch (Exception e) {
				this.vista.dialogo("Ocurrio una excepcion al intentar cerrar en el archivo remoto!", "Cerrar archivo...", JOptionPane.ERROR_MESSAGE);
			}
		} catch (NullPointerException e) {
			this.vista.dialogo("No hay archivos para cerrar!", "Cerrar archivo...", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void cerrar_archivo_local() {
		try {
			ComboItem item = this.vista.getCombo_cerrar_local();

			int fd = item.getFd();
			String nombre = item.getNombre();
			String tipo = item.getTipo();

			try {
				this.modelo.quita_archivo_local_abierto(fd);
				this.vista.borraItem_comboCerrar_local(fd);
				this.vista.borraItem_combo_archivo_local(fd);
				this.vista.borraItem_comboRW(fd, nombre, tipo);
				this.vista.actualiza_texto("");
			} catch (Exception e) {
				this.vista.dialogo("Ocurrio una excepcion al intentar cerrar en el archivo remoto!", "Cerrar archivo...", JOptionPane.ERROR_MESSAGE);
			}
		} catch (NullPointerException e) {
			this.vista.dialogo("No hay archivos para cerrar!", "Cerrar archivo...", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void volcar_datos_local_a_remoto() {
		try {
			int i;
			ComboItem item_local = this.vista.getCombo_archivo_local();
			int fd_local = item_local.getFd();
			File archivo_local = this.modelo.obtiene_archivo_local_abierto(fd_local);
			FileInputStream lector = new FileInputStream(archivo_local);

			ComboItem item_remoto = this.vista.getCombo_archivo_remoto();
			int fd_remoto = item_remoto.getFd();

			try {
				do {
					String texto = "";
					int caracter = -1;
					i = 0;

					while ((i < TOPE_BUFFER) && ((caracter = lector.read()) != -1)) {
						texto = texto + (char) caracter;
						i++;
					}

					int resultado = this.objetoRemoto.RFSWrite(fd_remoto, texto);
					
					if (resultado == -1) {
						this.vista.dialogo("Ocurrio un error en la escritura!", "Local a remoto...", JOptionPane.ERROR_MESSAGE);
					}
				} while (i == TOPE_BUFFER);
			} catch (Exception e) {
				lector.close();
				this.vista.dialogo("Ocurrio un error en la lectura/escritura!", "Local a remoto...", JOptionPane.ERROR_MESSAGE);
			}
		} catch (Exception e) {
			this.vista.dialogo("Ocurrio un error en el volcado!", "Local a remoto...", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void volcar_datos_remoto_a_local() {
		try {
			ComboItem item_local = this.vista.getCombo_archivo_local();
			int fd_local = item_local.getFd();
			File archivo_local = this.modelo.obtiene_archivo_local_abierto(fd_local);
			FileOutputStream escritor = new FileOutputStream(archivo_local, true);

			ComboItem item_remoto = this.vista.getCombo_archivo_remoto();
			int fd_remoto = item_remoto.getFd();

			// Llama al objeto remoto.
			int estado = 0;
			String texto = "";
			do {
				Object resultado = this.objetoRemoto.RFSRead(fd_remoto, TOPE_BUFFER);
				estado = ((RespuestaLectura) resultado).getCantidad();
				texto = ((RespuestaLectura) resultado).getLectura();
				if (estado != -1) {
					for (int i = 0; i < texto.length(); i++) {
						try {
							escritor.write((byte) texto.charAt(i));
						} catch (IOException e) {
							escritor.close();
							e.printStackTrace();
							this.vista.dialogo("Ocurrio un error al intentar leer en el archivo remoto!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
							break;
						}	
					}
				} else {
					escritor.close();
					this.vista.dialogo("Ocurrio un error al intentar leer en el archivo remoto!", "Leer archivo...", JOptionPane.ERROR_MESSAGE);
					break;
				}
			} while (estado == TOPE_BUFFER);
		} catch (Exception e) {
			e.printStackTrace();
			this.vista.dialogo("Ocurrio un error en el volcado!", "Remoto a local...", JOptionPane.ERROR_MESSAGE);
		}
	}
}
