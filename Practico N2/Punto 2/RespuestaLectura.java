import java.io.Serializable;

public class RespuestaLectura implements Serializable {
	private static final long serialVersionUID = 7L;
    private int cantidad;
    private String lectura;
    
    public RespuestaLectura(int cantidad, String lectura) {
        this.cantidad = cantidad;
        this.lectura = lectura;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getLectura() {
        return lectura;
    }

    public void setLectura(String lectura) {
        this.lectura = lectura;
    }
}