/*
 * InterfaceRemota.java
 *
 * Created on 27 de abril de 2004, 21:17
 */

//package chuidiang.ejemplos.rmi.suma;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface remota con los metodos que se pueden llamar en remoto
 * @author  Javier Abellon
 */
public interface InterfaceRemota extends Remote {
    public int RFSOpen(String nombre) throws RemoteException; 
    public RespuestaLectura RFSRead(int fd, int cantidad) throws RemoteException; 
    public int RFSWrite(int fd, String texto) throws RemoteException; 
    public int RFSClose(int fd) throws RemoteException;
}
